/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package recursos;

import java.util.ArrayList;

/**
 *
 * @author Jesús
 */
public  class ListaEstados {

    public static ArrayList<String> getEstadosMexico() {
        ArrayList<String> estados = new ArrayList<>();

        estados.add("Aguascalientes");
        estados.add("Baja California");
        estados.add("Baja California Sur");
        estados.add("Campeche");
        estados.add("Chiapas");
        estados.add("Chihuahua");
        estados.add("Coahuila de Zaragoza");
        estados.add("Colima");
        estados.add("Durango");
        estados.add("Estado de México");
        estados.add("Guanajuato");
        estados.add("Guerrero");
        estados.add("Hidalgo");
        estados.add("Jalisco");
        estados.add("Michoacán de Ocampo");
        estados.add("Morelos");
        estados.add("Nayarit");
        estados.add("Nuevo León");
        estados.add("Oaxaca");
        estados.add("Puebla");
        estados.add("Querétaro");
        estados.add("Quintana Roo");
        estados.add("San Luis Potosí");
        estados.add("Sinaloa");
        estados.add("Sonora");
        estados.add("Tabasco");
        estados.add("Tamaulipas");
        estados.add("Tlaxcala");
        estados.add("Veracruz de Ignacio de la Llave");
        estados.add("Yucatán");
        estados.add("Zacatecas");
        
        return estados;
    }
}
