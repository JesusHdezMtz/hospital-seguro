/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controladores;

import controladores.interfaces.AccionInterfaz;
import hospital.core.model.Medico;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javax.swing.JOptionPane;
import persistencia.interfaces.clases.MedicamentoInterfaz;
import persistencia.interfaces.clases.MedicoInterfaz;

/**
 * FXML Controller class
 *
 * @author Jahir
 */
public class BusquedaMedicoController implements Initializable, AccionInterfaz {

    @FXML
    private Button btnEditarMedico;

    @FXML
    private Button btnRegistrarMedico;

    @FXML
    private TableView<Medico> tablaMedicos;

    @FXML
    private TableColumn colNombre;

    @FXML
    private TableColumn colPaterno;

    @FXML
    private TableColumn colMaterno;

    @FXML
    private TableColumn colCedula;

    @FXML
    private TextField barraBusqueda;

    private ArrayList<Medico> medicoTodos = new ArrayList<>();
    private ArrayList<Medico> medicoFiltrado = new ArrayList<>();

    @FXML
    void editarMedico(ActionEvent event) throws IOException {
        Medico medico = (Medico) tablaMedicos.getSelectionModel().getSelectedItem();
        if (medico != null) {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/vista/RegistroMedico.fxml"));
            loader.load();
            RegistroMedicoController registroMed = loader.getController();
            registroMed.asignaParametrosEditar(this, medico, 2);
            Parent p = loader.getRoot();
            Stage s = new Stage();
            s.setScene(new Scene(p));
            s.show();
        } else {
            JOptionPane.showMessageDialog(null, "Seleccione un médico");
        }
    }

    @FXML
    void registrarMedico(ActionEvent event) throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/vista/RegistroMedico.fxml"));
        loader.load();
        RegistroMedicoController registroMed = loader.getController();
        registroMed.asignaParametros(this, 1);
        Parent p = loader.getRoot();
        Stage s = new Stage();
        s.setScene(new Scene(p));
        s.show();
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {

        colNombre.setCellValueFactory(new PropertyValueFactory<Medico, String>("nombre"));
        colPaterno.setCellValueFactory(new PropertyValueFactory<Medico, String>("apellidoPaterno"));
        colMaterno.setCellValueFactory(new PropertyValueFactory<Medico, String>("apellidoMaterno"));
        colCedula.setCellValueFactory(new PropertyValueFactory<Medico, Integer>("cedulaProfesional"));

        barraBusqueda.textProperty().addListener(new ChangeListener() {
            @Override
            public void changed(ObservableValue observable, Object oldValue, Object newValue) {
                filtraMedicos(newValue.toString());
            }
        });

        recuperaMedicos();
    }

    @FXML
    public void recuperaMedicos() {
        Medico medico = new Medico();
        medico.setPersistencia(new MedicoInterfaz());
        List<Medico> medicosRecuperados = new ArrayList<>();

        medicosRecuperados.addAll(medico.todosMedicos());
        medicoTodos.clear();
        medicoFiltrado.clear();
        medicoTodos.addAll(medicosRecuperados);
        medicoFiltrado.addAll(medicosRecuperados);
        cargaMedicosALaTabla((ArrayList<Medico>) medicosRecuperados);
    }

    @FXML
    public void filtraMedicos(String cadena) {
        medicoFiltrado.clear();
        if (!medicoTodos.isEmpty() && medicoTodos != null) {
            medicoTodos.forEach((medi) -> {
                if (medi.getNombre().toLowerCase().contains(cadena.toLowerCase())
                        || medi.getApellidoPaterno().toLowerCase().contains(cadena.toLowerCase())
                        || medi.getApellidoMaterno().toLowerCase().contains(cadena.toLowerCase())) {
                    medicoFiltrado.add(medi);
                }
            });
            cargaMedicosALaTabla(medicoFiltrado);
        }
    }

    @FXML
    public void cargaMedicosALaTabla(ArrayList<Medico> medicos) {
        ObservableList<Medico> datos = FXCollections.observableArrayList(medicos);
        tablaMedicos.getItems().clear();
        tablaMedicos.setItems(datos);

    }

    @Override
    public void actualizaTabla() {
        recuperaMedicos();
    }
}
