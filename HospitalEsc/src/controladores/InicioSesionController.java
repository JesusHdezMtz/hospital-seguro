/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controladores;

import hospital.core.model.Usuario;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import persistencia.interfaces.clases.UsuarioInterfaz;
import javax.swing.JOptionPane;

/**
 *
 * @author Jahir
 */
public class InicioSesionController implements Initializable {

    @FXML
    private Button botonLogin;

    @FXML
    private TextField usuarioTxt;

    @FXML
    private TextField contrasenaTxt;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    @FXML
    void login(ActionEvent event) throws IOException, NoSuchAlgorithmException {
        /*
        Tipo acceso 1: Recepcionista
        Tipo acceso 2: Medico
        Tipo acceso 3: Administrador
         */
        String user = usuarioTxt.getText();
        String password = contrasenaTxt.getText();

        Usuario usuarioBuscador = new Usuario(new UsuarioInterfaz());
        Usuario usuarioResultado = usuarioBuscador.buscarUsuario(user);
        if (usuarioResultado != null) {
            String passwordHash = hashPassword(password);

            if (usuarioResultado.getUsuario().equals(user)
                    && usuarioResultado.getContrasena().equals(passwordHash)) {

                switch (usuarioResultado.getTipoAcceso()) {
                    case 1:
                        Parent root = FXMLLoader.load(getClass()
                                .getResource("/vista/PrincipalRecepcionista.fxml"));
                        Scene scene = new Scene(root);
                        Stage stage = new Stage();
                        stage.setScene(scene);
                        stage.show();
                        Stage stageActual = (Stage) botonLogin.getScene().getWindow();
                        stageActual.close();
                        break;
                    case 2:
                        FXMLLoader loader = new FXMLLoader();
                        loader.setLocation(getClass().getResource("/vista/PrincipalMedico.fxml"));
                        loader.load();
                        PrincipalMedicoController principalMedico = loader.getController();
                        principalMedico.asignaParametro(usuarioResultado);
                        Parent p = loader.getRoot();
                        Stage s = new Stage();
                        s.setScene(new Scene(p));
                        s.show();
                        Stage stageLogin = (Stage) botonLogin.getScene().getWindow();
                        stageLogin.close();
                        break;
                    case 3:
                        Parent rootAdmi = FXMLLoader.load(getClass()
                                .getResource("/vista/PrincipalAdministrador.fxml"));
                        Scene sceneAdmi = new Scene(rootAdmi);
                        Stage stageAdmi = new Stage();
                        stageAdmi.setScene(sceneAdmi);
                        stageAdmi.show();
                        Stage stageActualAdmi = (Stage) botonLogin.getScene().getWindow();
                        stageActualAdmi.close();
                        break;
                }
            } else {
                JOptionPane.showMessageDialog(null, "Usuario y/o contraseña incorrectos");
            }
        } else {
            JOptionPane.showMessageDialog(null, "Usuario y/o contraseña incorrectos");
        }
    }

    public String hashPassword(String contrasena) throws NoSuchAlgorithmException,
            UnsupportedEncodingException {
        MessageDigest sha256 = MessageDigest.getInstance("SHA-256");
        sha256.update(contrasena.getBytes("UTF-8"));
        byte[] digest = sha256.digest();
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < digest.length; i++) {
            sb.append(String.format("%02x", digest[i]));
        }
        return sb.toString();
    }
}
