/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controladores;

import hospital.core.model.Paciente;
import hospital.core.model.Seguro;
import java.net.URL;
import java.time.ZoneId;
import java.util.List;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javax.swing.JOptionPane;
import persistencia.interfaces.clases.PacienteInterfaz;
import persistencia.interfaces.clases.SeguroInterfaz;

/**
 * FXML Controller class
 *
 * @author Jahir
 */
public class RegistroPacienteController implements Initializable {

    @FXML
    private Button btnGuardar;    
    @FXML
    private TextField tfNombre;
    @FXML
    private TextField tfApellidoPaterno;
    @FXML
    private TextField tfApellidoMaterno;
    @FXML
    private TextField tfCurp;
    @FXML
    private TextField tfTelefono;
    @FXML
    private TextField tfDireccion;
    @FXML
    private TextField tfCodigoPostal;
    @FXML
    private TextField tfLocalidad;
    @FXML
    private TextField tfNumeroSeguro;
    @FXML
    private TextField tfEstado;
    @FXML
    private ComboBox<Seguro> cbTipoSeguro;
    @FXML
    private DatePicker dpFechaNacimiento;
    @FXML
    private ObservableList<Seguro> obsTipoSeguro;
    
    
    Stage stagemaster;
    private boolean banderaEditarPaciente = false;
    private int idPaciente;

    @FXML
    public void btnGuardar() {
        if (!validarCamposVacios()) {
            if (!banderaEditarPaciente) {
                Paciente paciente = crearPaciente();
                paciente.nuevoPaciente();
                JOptionPane.showMessageDialog(null, "Paciente registrado correctamente");
            } else {
                Paciente paciente = crearPaciente();
                paciente.setId(idPaciente);
                paciente.editarPaciente();
                JOptionPane.showMessageDialog(null, "Paciente editado correctamente");
            }
        } else {
            JOptionPane.showMessageDialog(null, "Completa los campos vacíos");
        }
    }

    public Paciente crearPaciente() {
        hospital.core.model.Paciente paciente = new hospital.core.model.Paciente();
        paciente.setNombre(tfNombre.getText());
        paciente.setApellidoPaterno(tfApellidoPaterno.getText());
        paciente.setApellidoMaterno(tfApellidoMaterno.getText());
        paciente.setCodigoPostal(tfCodigoPostal.getText());
        paciente.setCurp(tfCurp.getText());
        paciente.setDireccion(tfDireccion.getText());
        paciente.setLocalidad(tfLocalidad.getText());
        paciente.setNumeroSeguro(Integer.parseInt(tfNumeroSeguro.getText()));
        paciente.setNumTelefono(tfTelefono.getText());
        paciente.setFechaNacimiento(java.sql.Date.valueOf(dpFechaNacimiento.
                getValue()));
        paciente.setEstado(tfEstado.getText());
        paciente.setSeguro(cbTipoSeguro.getValue());
        paciente.setPersistencia(new PacienteInterfaz());
        return paciente;
    }                

    public void llenarComboTipoSeguro() {
        Seguro seguro = new Seguro(new SeguroInterfaz());
        List<Seguro> listaSeguro = seguro.todosSeguro();
        obsTipoSeguro = FXCollections.observableArrayList(listaSeguro);
        cbTipoSeguro.setItems(obsTipoSeguro);
    }

    public boolean validarCamposVacios() {
        boolean bandera = false;
        if (tfNombre.getText().isEmpty()
                || tfApellidoPaterno.getText().isEmpty()
                || tfApellidoMaterno.getText().isEmpty()
                || tfCodigoPostal.getText().isEmpty()
                || tfCurp.getText().isEmpty()
                || tfDireccion.getText().isEmpty()
                || tfLocalidad.getText().isEmpty()
                || tfNumeroSeguro.getText().isEmpty()
                || tfTelefono.getText().isEmpty()
                || dpFechaNacimiento.getValue() == null
                || tfEstado.getText().isEmpty()
                || cbTipoSeguro.getSelectionModel().isEmpty()) {
            bandera = true;
        }
        return bandera;
    }

    public void recibeStage(Stage stage) {
        stagemaster = stage;
    }
    
    public void recibePacienteSeleccionado(Paciente paciente, Stage stage){
        banderaEditarPaciente = true;
        stagemaster = stage;
        idPaciente = paciente.getId();
        tfNombre.setText(paciente.getNombre());
        tfApellidoPaterno.setText(paciente.getApellidoPaterno());
        tfApellidoMaterno.setText(paciente.getApellidoMaterno());
        tfCodigoPostal.setText(paciente.getCodigoPostal());
        tfCurp.setText(paciente.getCurp());
        tfDireccion.setText(paciente.getDireccion());
        tfEstado.setText(paciente.getEstado());
        tfLocalidad.setText(paciente.getLocalidad());
        tfNumeroSeguro.setText(Integer.toString(paciente.getNumeroSeguro()));
        tfTelefono.setText(paciente.getNumTelefono());
        Seguro seguro = paciente.getSeguro();
        cbTipoSeguro.getSelectionModel().select(seguro);

        dpFechaNacimiento.setValue(paciente.getFechaNacimiento().
               toInstant().atZone(ZoneId.systemDefault()).toLocalDate());        
        llenarComboTipoSeguro();
    }        
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        llenarComboTipoSeguro();
    }

}
