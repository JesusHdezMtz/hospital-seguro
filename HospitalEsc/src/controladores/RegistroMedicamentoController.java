/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controladores;

import controladores.interfaces.AccionInterfaz;
import hospital.core.model.Medicamento;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javax.swing.JOptionPane;
import persistencia.interfaces.clases.MedicamentoInterfaz;

/**
 * FXML Controller class
 *
 * @author Jahir
 */
public class RegistroMedicamentoController implements Initializable {

    @FXML
    private TextField campoNombre;

    @FXML
    private TextField campoComponente;

    @FXML
    private TextField campoPresentacion;

    @FXML
    private Button botonGuardar;

    private AccionInterfaz accionInterfaz;
    private Medicamento medicamento;
    private int flag;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    @FXML
    void guardarMedicamento(ActionEvent event) {
        String nombre = campoNombre.getText();
        String componente = campoComponente.getText();
        String presentacion = campoPresentacion.getText();

        if (nombre.isEmpty() || componente.isEmpty() || presentacion.isEmpty()) {
            JOptionPane.showMessageDialog(null, "Campos vacios");
        } else {
            if (flag == 1) {
                Medicamento medicamento = new Medicamento(new MedicamentoInterfaz());
                medicamento.setNombre(nombre);
                medicamento.setComponenteActivo(componente);
                medicamento.setPresentacion(presentacion);
                medicamento.nuevoMedicamento();
                accionInterfaz.actualizaTabla();
                JOptionPane.showMessageDialog(null, "Medicamento registrado con éxito");
                campoNombre.setText("");
                campoComponente.setText("");
                campoPresentacion.setText("");
            }
            if (flag == 2) {
                medicamento.setNombre(nombre);
                medicamento.setPresentacion(presentacion);
                medicamento.setComponenteActivo(componente);
                medicamento.setPersistencia(new MedicamentoInterfaz());
                medicamento.editarMedicamento();
                accionInterfaz.actualizaTabla();
                JOptionPane.showMessageDialog(null, "Medicamento actualizado con éxito");
                Stage stageActualAdmi = (Stage) botonGuardar.getScene().getWindow();
                stageActualAdmi.close();
            }

        }
    }

    @FXML
    public void asignaParametros(AccionInterfaz accionInterfaz, int flag) {
        this.accionInterfaz = accionInterfaz;
        this.flag = flag;
    }

    public void asignaParametrosEditar(AccionInterfaz accionInterfaz, Medicamento medicamento, int flag) {
        this.accionInterfaz = accionInterfaz;
        this.medicamento = medicamento;
        this.flag = flag;

        campoComponente.setText(medicamento.getComponenteActivo());
        campoNombre.setText(medicamento.getNombre());
        campoPresentacion.setText(medicamento.getPresentacion());
    }

}
