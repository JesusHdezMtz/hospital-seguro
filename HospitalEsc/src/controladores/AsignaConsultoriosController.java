/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controladores;

import hospital.core.model.Cola;
import hospital.core.model.Consultorio;
import hospital.core.model.Paciente;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import javax.swing.JOptionPane;
import persistencia.controladores.ColaJpaController;
import persistencia.controladores.ConsultorioJpaController;
import persistencia.controladores.PacienteJpaController;
import persistencia.controladores.exceptions.NonexistentEntityException;
import persistencia.interfaces.clases.ConsultorioInterfaz;

/**
 * FXML Controller class
 *
 * @author Jahir
 */
public class AsignaConsultoriosController implements Initializable {

    @FXML
    private Label labelNombre;
    @FXML
    private ComboBox<Consultorio> cbConsultorios;
    @FXML
    private TableView<Consultorio> tablaConsultorios;
    @FXML
    private TableColumn<Consultorio, String> columnNombre;
    @FXML
    private TableColumn<Consultorio, String> columnPacientesEnEspera;
    @FXML
    private ObservableList<Consultorio> obsConsultorios;
    
    
    private Paciente pacienteG;
    private Stage stagemaster;

    public void recibePacienteSeleccionado(Paciente paciente, Stage stage) {
        stagemaster = stage;
        pacienteG = paciente;
        labelNombre.setText(paciente.getNombre() + " "
                + paciente.getApellidoPaterno() + " "
                + paciente.getApellidoMaterno());
    }
    
    @FXML
    public void btnAsignar(){
        if(cbConsultorios.getSelectionModel().getSelectedItem() == null){
            JOptionPane.showMessageDialog(null, "Selecciona un consultorio");
        }else{            
            try {
                PacienteJpaController pacienteJpa = new PacienteJpaController();
                ColaJpaController colajpa = new ColaJpaController();
                ConsultorioJpaController consultorioJpa = new ConsultorioJpaController();

                persistencia.entidades.Consultorio consultorio = consultorioJpa.
                        findConsultorio(cbConsultorios.getValue().getId());
                persistencia.entidades.Cola cola = colajpa.
                        findCola(consultorio.getIdcola().getIdcola());
                System.out.println("El id de la cola final es " + cola.getIdcola());
                persistencia.entidades.Paciente paciente
                        = pacienteJpa.findPaciente(pacienteG.getId());
                paciente.setIdcola(cola);
                pacienteJpa.edit(paciente);
                JOptionPane.showMessageDialog(null, "Paciente asignado correctamente");
            } catch (NonexistentEntityException ex) {
                ex.printStackTrace();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }
    
    @FXML
    public void llenarComboConsultorios() {
        Consultorio consultorio = new Consultorio(new ConsultorioInterfaz());
        List<Consultorio> listaConsultorios = consultorio.todosConsultorios();
        obsConsultorios = FXCollections.observableArrayList(listaConsultorios);
        cbConsultorios.setItems(obsConsultorios);
    }
    
    public void llenarTablaConsultorios(){
        Consultorio consultorio = new Consultorio(new ConsultorioInterfaz());
        List<Consultorio> listaConsultorios = consultorio.todosConsultorios();        
        ObservableList<Consultorio> obsListConsultorios = FXCollections.
                observableArrayList(listaConsultorios);
        columnNombre.setCellValueFactory(new PropertyValueFactory
                <Consultorio, String> ("numero"));
        columnPacientesEnEspera.setCellValueFactory(new PropertyValueFactory
                <Consultorio,String> ("tamanio"));        
        tablaConsultorios.setItems(obsListConsultorios);
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        //llenarTablaConsultorios();
        llenarComboConsultorios();
    }

}
