/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controladores;

import controladores.interfaces.AccionInterfaz;
import hospital.core.model.Medicamento;
import java.awt.event.InputMethodEvent;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javax.swing.JOptionPane;
import persistencia.interfaces.clases.MedicamentoInterfaz;

/**
 * FXML Controller class
 *
 * @author Jahir
 */
public class BusquedaMedicamentoController implements Initializable, AccionInterfaz {

    @FXML
    private Button botonEditarMedicamento;

    @FXML
    private Button botonRegistrarMedicamento;

    @FXML
    private TableView tablaMedcamentos;

    @FXML
    private TableColumn columnaNombre;

    @FXML
    private TableColumn columnaPresentacion;

    @FXML
    private TableColumn columnaComponente;

    @FXML
    private TextField barraBusquedaMedicamento;

    private ArrayList<Medicamento> medicamentosTodos = new ArrayList<>();
    private ArrayList<Medicamento> medicamentosFiltrados = new ArrayList<>();

    @Override
    public void initialize(URL url, ResourceBundle rb) {

        columnaNombre.setCellValueFactory(new PropertyValueFactory<Medicamento, String>("nombre"));
        columnaComponente.setCellValueFactory(new PropertyValueFactory<Medicamento, String>("componenteActivo"));
        columnaPresentacion.setCellValueFactory(new PropertyValueFactory<Medicamento, String>("Presentacion"));

        barraBusquedaMedicamento.textProperty().addListener(new ChangeListener() {
            @Override
            public void changed(ObservableValue observable, Object oldValue, Object newValue) {
                filtraMedicamentos(newValue.toString());
            }
        });

        recuperaMedicamentos();
    }

    @FXML
    public void filtraMedicamentos(String cadena) {
        medicamentosFiltrados.clear();
        if (!medicamentosTodos.isEmpty() && medicamentosTodos != null) {
            medicamentosTodos.forEach((medi) -> {
                if (medi.getNombre().toLowerCase().contains(cadena.toLowerCase())) {
                    medicamentosFiltrados.add(medi);
                }
            });
            cargaMedicamentosALaTabla(medicamentosFiltrados);
        }
    }

    @FXML
    public void recuperaMedicamentos() {
        Medicamento medicamento = new Medicamento(new MedicamentoInterfaz());
        List<Medicamento> medicamentosRecuperados = new ArrayList<>();

        medicamentosRecuperados.addAll(medicamento.todosMedicamentos());
        medicamentosTodos.clear();
        medicamentosFiltrados.clear();
        medicamentosTodos.addAll(medicamentosRecuperados);
        medicamentosFiltrados.addAll(medicamentosRecuperados);
        cargaMedicamentosALaTabla((ArrayList<Medicamento>) medicamentosRecuperados);
    }

    @FXML
    public void cargaMedicamentosALaTabla(ArrayList<Medicamento> medicamentos) {
        ObservableList<Medicamento> datos = FXCollections.observableArrayList(medicamentos);
        tablaMedcamentos.getItems().clear();
        tablaMedcamentos.setItems(datos);

    }

    @FXML
    void registrarMedicamento(ActionEvent event) throws IOException {

        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/vista/RegistroMedicamento.fxml"));
        loader.load();
        RegistroMedicamentoController registroMed = loader.getController();
        registroMed.asignaParametros(this, 1);
        Parent p = loader.getRoot();
        Stage s = new Stage();
        s.setScene(new Scene(p));
        s.show();

    }

    @Override
    public void actualizaTabla() {
        recuperaMedicamentos();
    }

    @FXML
    void editarMedicamento(ActionEvent event) throws IOException {
        Medicamento medicamento = (Medicamento) tablaMedcamentos.getSelectionModel().getSelectedItem();
        if (medicamento != null) {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/vista/RegistroMedicamento.fxml"));
            loader.load();
            RegistroMedicamentoController registroMed = loader.getController();
            registroMed.asignaParametrosEditar(this, medicamento, 2);
            Parent p = loader.getRoot();
            Stage s = new Stage();
            s.setScene(new Scene(p));
            s.show();
        } else {
            JOptionPane.showMessageDialog(null, "Seleccione un medicamento");
        }
    }

}
