/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controladores;

import hospital.core.model.Consulta;
import hospital.core.model.Medicamento;
import hospital.core.model.Medico;
import hospital.core.model.Paciente;
import hospital.core.model.Receta;
import hospital.core.model.Usuario;
import java.io.IOException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.swing.JOptionPane;
import persistencia.controladores.ConsultaJpaController;
import persistencia.controladores.ConsultorioJpaController;
import persistencia.controladores.MedicoJpaController;
import persistencia.controladores.RecetaJpaController;
import persistencia.entidades.Cola;
import persistencia.entidades.Consultorio;
import persistencia.entidades.Itinerario;
import persistencia.interfaces.clases.ConsultaInterfaz;
import persistencia.interfaces.clases.MedicoInterfaz;
import persistencia.interfaces.clases.RecetaInterfaz;
import utilidades.*;

/**
 * FXML Controller class
 *
 * @author andres
 */
public class PrincipalMedicoController implements Initializable {

    @FXML
    private Label consultorioNombre;

    @FXML
    private Label consultorioCURP;

    @FXML
    private Label consultorioSeguro;

    @FXML
    private Label consultorioNacimiento;

    @FXML
    private TableColumn<persistencia.entidades.Receta, String> columnaPeso;

    @FXML
    private TableColumn<persistencia.entidades.Receta, String> columnaEstatura;

    @FXML
    private TableColumn<persistencia.entidades.Receta, String> columnaPresion;

    @FXML
    private TableColumn<persistencia.entidades.Receta, String> columnaPrescripcion;

    @FXML
    private TableView<persistencia.entidades.Receta> tablaRecetas;

    @FXML
    private Button botonObtener;

    private persistencia.entidades.Paciente paciente;
    private Usuario usuario;
    private Medico medico;
    private persistencia.entidades.Medico eMedico;
    private Cola colaConsultorio;
    EntityManagerFactory emf = Persistence.createEntityManagerFactory("HospitalJPAPU");
    private persistencia.entidades.Consulta consulta;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        inicializarTabla();
    }

    private void recuperaCola() {
        Medico medicoConsult = new Medico();
        medicoConsult.setPersistencia(new MedicoInterfaz());
        ArrayList<Medico> medicos = (ArrayList<Medico>) medicoConsult.todosMedicos();
        medicos.forEach((medi) -> {
            if (medi.getUsuario().getUsuario().equals(usuario.getUsuario())) {
                medico = medi;
            }
        });
        MedicoJpaController jpa = new MedicoJpaController(emf);
        ConsultorioJpaController consultJpa = new ConsultorioJpaController(emf);
        persistencia.entidades.Medico medico = jpa.findMedico(this.medico.getId());
        eMedico = medico;
        Consultorio consultorio = null;
        for (int i = 0; i < medico.getItinerarioList().size(); i++) {
            if (medico.getItinerarioList().get(i).getMedico().getIdmedico() == medico.getIdmedico()) {
                consultorio = consultJpa.findConsultorio(medico.getItinerarioList().get(i).getConsultorio().getIdconsultorio());
            }
        }
        Cola cola = consultorio.getIdcola();
        this.colaConsultorio = cola;
        recuperaPrimerPaciente();

    }

    @FXML
    void obtenerPaciente(ActionEvent event) {
        recuperaCola();
    }

    private void recuperaPrimerPaciente() {
        if (!colaConsultorio.getConsultorioList().isEmpty()) {
            paciente = colaConsultorio.getPacienteList().get(0);
            asignaPaciente(paciente);

        } else {
            JOptionPane.showMessageDialog(null, "Ya no hay pacientes en espera");

        }
    }

    public void asignaPaciente(persistencia.entidades.Paciente paciente) {
        consultorioCURP.setText(paciente.getCurp());
        DateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        consultorioNacimiento.setText(format.format(paciente.getFechaNacimiento()));
        consultorioNombre.setText(paciente.getNombre());
        consultorioSeguro.setText(paciente.getIdseguro().getNombre());
        cargarReecetas(paciente);

    }

    public void cargarReecetas(persistencia.entidades.Paciente paciente) {
        ArrayList<persistencia.entidades.Receta> recetas = new ArrayList<>();
        RecetaJpaController jpaRec = new RecetaJpaController(emf);
        for (int i = 0; i < paciente.getConsultaList().size(); i++) {
            if (paciente.getConsultaList().get(i).getRecetaIdreceta() != null) {
                recetas.add(jpaRec.findReceta(paciente.getConsultaList().get(i).getRecetaIdreceta().getIdreceta()));
            }
        }
        ObservableList<persistencia.entidades.Receta> datos = FXCollections.observableArrayList(recetas);
        tablaRecetas.getItems().clear();
        tablaRecetas.setItems(datos);
        creaConsulta();
    }

    public void creaConsulta() {
        ConsultaJpaController jpaConsult = new ConsultaJpaController(emf);
        persistencia.entidades.Consulta consultaCrea = new persistencia.entidades.Consulta();
        consultaCrea.setFechaYHora(new Date());
        consultaCrea.setIdmedico(eMedico);
        consultaCrea.setIdconsulta(jpaConsult.getConsultaCount() + 1);
        consultaCrea.setIdpaciente(paciente);
        consulta = consultaCrea;
        jpaConsult.create(consultaCrea);

    }

    private void inicializarTabla() {
        columnaPeso.setCellValueFactory(new PropertyValueFactory<>("peso"));
        columnaEstatura.setCellValueFactory(new PropertyValueFactory<>("estatura"));
        columnaPrescripcion.setCellValueFactory(new PropertyValueFactory<>("indicacion"));
        columnaPresion.setCellValueFactory(new PropertyValueFactory<>("presion"));
    }

    @FXML
    public void asignaParametro(Usuario usuario) {
        this.usuario = usuario;
    }

    @FXML
    void RegistrarReeceta(ActionEvent event) throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/vista/RegistroReceta.fxml"));
        loader.load();
        RegistroRecetaController registroRec = loader.getController();
        registroRec.asignaParametros(paciente, eMedico, consulta);
        Parent p = loader.getRoot();
        Stage s = new Stage();
        s.setScene(new Scene(p));
        s.show();
    }

    @FXML
    void concluirConsulta(ActionEvent event) {

    }

}
