/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controladores;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import persistencia.controladores.MedicamentoJpaController;
import persistencia.entidades.Consulta;
import persistencia.entidades.Medicamento;
import persistencia.entidades.Medico;
import persistencia.entidades.Paciente;

/**
 * FXML Controller class
 *
 * @author andres
 */
public class RegistroRecetaController implements Initializable {

    @FXML
    private TextField inputPeso;

    @FXML
    private TextField inputEstatura;

    @FXML
    private TextField inputPresion;

    @FXML
    private TextArea areaIndicaciones;

    @FXML
    private Button guardarReceta;

    @FXML
    private ComboBox<String> comboMedicamentos;

    @FXML
    private ListView<String> listMedicamentos;

    EntityManagerFactory emf = Persistence.createEntityManagerFactory("HospitalJPAPU");
    private Paciente paciente;
    private Medico medico;
    private Consulta consulta;
    List<Medicamento> medis = new ArrayList<>();
    ArrayList<String> nombreMedis = new ArrayList<>();

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
    }

    public void  inicio(){
        MedicamentoJpaController jpa = new MedicamentoJpaController(emf);
        for (int i = 0; i < jpa.getMedicamentoCount(); i++) {
            medis.add(jpa.findMedicamento(i+1));
        }
        ArrayList<String> nombres = new ArrayList<>();
        for (int i = 0; i < medis.size(); i++) {
            nombres.add(medis.get(i).getNombre());

        }
        ObservableList<String> estados = FXCollections.observableArrayList(nombres);
        comboMedicamentos.setItems(estados);

        comboMedicamentos.valueProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                agregarATabla(newValue);
            }
        });
    }
    public void agregarATabla(String medicamento) {
        nombreMedis.add(medicamento);
        ObservableList<String> items = FXCollections.observableArrayList(nombreMedis);
        listMedicamentos.getItems().clear();
        listMedicamentos.setItems(items);
    }

    @FXML
    public void asignaParametros(Paciente paciente, Medico medico, Consulta consulta) {
        this.paciente = paciente;
        this.medico = medico;
        this.consulta = consulta;
        inicio();
    }

    @FXML
    void guardarReceta(ActionEvent event) {
        MedicamentoJpaController jpam = new MedicamentoJpaController(emf);
        String peso = inputPeso.getText();
        String estatura = inputEstatura.getText();
        String presion = inputPresion.getText();
        String observaciones = areaIndicaciones.getText();
        
        if(peso.isEmpty() || estatura.isEmpty() || presion.isEmpty() || observaciones.isEmpty() || nombreMedis.isEmpty()){
            
        }
    }

}
