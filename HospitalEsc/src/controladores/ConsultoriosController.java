/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controladores;

import hospital.core.model.Consultorio;
import hospital.core.model.Paciente;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javax.swing.JOptionPane;
import persistencia.interfaces.clases.ConsultorioInterfaz;

/**
 * FXML Controller class
 *
 * @author Jahir
 */
public class ConsultoriosController implements Initializable {

    @FXML
    private ComboBox<Consultorio> cbConsultorios;
    @FXML
    private TableView<Paciente> tablaPacientes;
    @FXML
    private TableColumn<Paciente, String> columnNombre;
    @FXML
    private TableColumn<Paciente, String> columnApellidoPaterno;
    @FXML
    private TableColumn<Paciente, String> columnApellidoMaterno;
    @FXML
    private ObservableList<Consultorio> obsConsultorios;

    @FXML
    public void btnVerPacientes() {
        if (cbConsultorios.getSelectionModel().getSelectedItem() == null) {
            JOptionPane.showMessageDialog(null, "Selecciona un consultorio");
        } else {
            llenarTablaPacientes();
        }
    }

    @FXML
    public void llenarTablaPacientes() {
        Consultorio consultorio = cbConsultorios.getValue();
        List<Paciente> listaPacientes = consultorio.getCola().getPacientes();

        ObservableList<Paciente> obsListPacientes = FXCollections.
                observableArrayList(listaPacientes);
        columnNombre.setCellValueFactory(new PropertyValueFactory<Paciente, String>("nombre"));
        columnApellidoPaterno.setCellValueFactory(new PropertyValueFactory<Paciente, String>("apellidoPaterno"));
        columnApellidoMaterno.setCellValueFactory(new PropertyValueFactory<Paciente, String>("apellidoMaterno"));
        tablaPacientes.setItems(obsListPacientes);
    }

    @FXML
    public void llenarComboConsultorios() {
        Consultorio consultorio = new Consultorio(new ConsultorioInterfaz());
        List<Consultorio> listaConsultorios = consultorio.todosConsultorios();
        obsConsultorios = FXCollections.observableArrayList(listaConsultorios);
        cbConsultorios.setItems(obsConsultorios);
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        llenarComboConsultorios();
    }

}
