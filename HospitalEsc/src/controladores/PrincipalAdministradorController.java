/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controladores;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Jahir
 */
public class PrincipalAdministradorController implements Initializable {

    @FXML
    private Pane panelPrincipal;
    
    @FXML
    private void menuAdministracionMedicos(){
        FXMLLoader loader = new FXMLLoader();
        AnchorPane root;
        try {
        root = (AnchorPane) FXMLLoader.load(getClass()
                .getResource("/vista/BusquedaMedico.fxml"));
        Scene scene = new Scene(root);
        Stage stage = new Stage();
        stage.setScene(scene);
        panelPrincipal.getChildren().setAll(root);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
    
    @FXML
    private void menuAdministracionMedicamento(){
        FXMLLoader loader = new FXMLLoader();
        AnchorPane root;
        try {
        root = (AnchorPane) FXMLLoader.load(getClass()
                .getResource("/vista/BusquedaMedicamento.fxml"));
        Scene scene = new Scene(root);
        Stage stage = new Stage();
        stage.setScene(scene);
        //panelPrincipal.set
        panelPrincipal.getChildren().setAll(root);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        menuAdministracionMedicos();
    }    
    
}
