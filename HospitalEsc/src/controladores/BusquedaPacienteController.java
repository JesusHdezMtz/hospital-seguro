/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controladores;

import hospital.core.model.Paciente;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javax.swing.JOptionPane;
import persistencia.interfaces.clases.PacienteInterfaz;

/**
 * FXML Controller class
 *
 * @author Jahir
 */
public class BusquedaPacienteController implements Initializable {

    @FXML
    private Button btnRegistrarPaciente;
    @FXML 
    private Button btnEditarPaciente;
    @FXML
    private Button btnBuscarPaciente;
    @FXML
    private TextField tfBusqueda;    
    @FXML
    private TableView<Paciente> tablaPacientes;
    @FXML
    private TableColumn<Paciente, String> columnNombre;
    @FXML
    private TableColumn<Paciente, String> columnApellidoPaterno;
    @FXML
    private TableColumn<Paciente, String> columnApellidoMaterno;
    @FXML
    private TableColumn<Paciente, String> columnDireccion;
    
    @FXML
    public void tfBuscarPacientes(){
        hospital.core.model.Paciente paciente = new hospital.
                core.model.Paciente(new PacienteInterfaz());
        List<Paciente> listaPacientes = paciente.todosPaciente();
        List<Paciente> pacientesFiltrados = new ArrayList<>();
        for (int i = 0; i < listaPacientes.size(); i++) {
            if(listaPacientes.get(i).getNombre().
                    contains(tfBusqueda.getText())){
                pacientesFiltrados.add(listaPacientes.get(i));
            }            
        }
        ObservableList<Paciente> obsListPaciente = FXCollections.
                observableArrayList(pacientesFiltrados);
        columnNombre.setCellValueFactory(new PropertyValueFactory
                <Paciente,String> ("nombre"));
        columnApellidoPaterno.setCellValueFactory(new PropertyValueFactory
                <Paciente,String> ("apellidoPaterno"));
        columnApellidoMaterno.setCellValueFactory(new PropertyValueFactory
                <Paciente,String> ("apellidoMaterno"));
        columnDireccion.setCellValueFactory(new PropertyValueFactory
                <Paciente,String> ("direccion"));
        tablaPacientes.setItems(obsListPaciente);
    }
    
    @FXML
    private void btnRegistrarPaciente(){
        try {
            FXMLLoader loader = new FXMLLoader();
            AnchorPane root = (AnchorPane) loader.load(getClass()
                    .getResource("/vista/RegistroPaciente.fxml").openStream());
            Scene scene = new Scene(root);
            Stage stage = new Stage();   
            stage.setScene(scene);
            RegistroPacienteController registroPacienteController = 
                    (RegistroPacienteController) loader.getController();
            registroPacienteController.recibeStage(stage);                       
            stage.setTitle("Registro del paciente");
            stage.show();
        } catch (IOException ex) {
            Logger.getLogger(BusquedaPacienteController.class.getName())
                    .log(Level.SEVERE, null, ex);
        }
    }        
    
    @FXML
    private void btnEditarPaciente(){
        Paciente paciente = tablaPacientes.getSelectionModel().getSelectedItem();
        if(tablaPacientes.getSelectionModel().getSelectedItem() != null){
            try {
                FXMLLoader loader = new FXMLLoader();
                AnchorPane root = (AnchorPane) loader.load(getClass()
                        .getResource("/vista/RegistroPaciente.fxml").openStream());
                RegistroPacienteController registroPacienteController
                        = (RegistroPacienteController) loader.getController();
                Scene scene = new Scene(root);
                Stage stage = new Stage();
                stage.setScene(scene);
                registroPacienteController.
                        recibePacienteSeleccionado(paciente, stage);
                stage.setTitle("Editar paciente");
                stage.show();
            } catch (IOException ex) {
                Logger.getLogger(BusquedaPacienteController.class.getName())
                        .log(Level.SEVERE, null, ex);
            }
        }else{
            JOptionPane.showMessageDialog(null, "Selecciona un paciente");
        }
    }
    
    @FXML
    private void AsignarPacienteAConsultorio(){
        Paciente paciente = tablaPacientes.getSelectionModel().getSelectedItem();
        if(tablaPacientes.getSelectionModel().getSelectedItem() != null){
            try {
                FXMLLoader loader = new FXMLLoader();
                AnchorPane root = (AnchorPane) loader.load(getClass()
                        .getResource("/vista/AsignaConsultorios.fxml").openStream());
                Scene scene = new Scene(root);
                Stage stage = new Stage();
                stage.setScene(scene);
                AsignaConsultoriosController colaConsultorioController
                        = (AsignaConsultoriosController) loader.getController();
                colaConsultorioController.recibePacienteSeleccionado(paciente, stage);
                stage.setTitle("Asignación de paciente a un consultorio");
                stage.show();
            } catch (IOException ex) {
                Logger.getLogger(BusquedaPacienteController.class.getName())
                        .log(Level.SEVERE, null, ex);
            }
        }else{
            JOptionPane.showMessageDialog(null, "Selecciona un paciente");
        }
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {        
        
    }    
    
}
