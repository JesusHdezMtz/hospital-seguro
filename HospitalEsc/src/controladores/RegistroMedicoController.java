/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controladores;

import controladores.interfaces.AccionInterfaz;
import hospital.core.model.Medico;
import hospital.core.model.Usuario;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.ResourceBundle;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import javafx.collections.FXCollections;
import javafx.stage.Stage;
import javax.swing.JOptionPane;
import persistencia.interfaces.clases.MedicoInterfaz;
import persistencia.interfaces.clases.UsuarioInterfaz;

/**
 * FXML Controller class
 *
 * @author Jahir
 */
public class RegistroMedicoController implements Initializable {

    @FXML
    private Button btnGuardar;

    @FXML
    private DatePicker campoFecha;

    @FXML
    private ComboBox comboEstados;

    @FXML
    private TextField campoCedula;

    @FXML
    private TextField campoLocalidad;

    @FXML
    private TextField campoCodigoPostal;

    @FXML
    private TextField campoDireccion;

    @FXML
    private TextField campoTelefono;

    @FXML
    private TextField campoCurp;

    @FXML
    private TextField campoMaterno;

    @FXML
    private TextField campoPaterno;

    @FXML
    private TextField campoNombre;

    @FXML
    private TextField campoContrasena;

    @FXML
    private TextField campoUsuario;

    private AccionInterfaz accionInterfaz;
    private int flag;
    private Medico medico;

    @FXML
    public void btnGuardar() throws NoSuchAlgorithmException, UnsupportedEncodingException {
        String cedula = campoCedula.getText();
        String codiPostal = campoCodigoPostal.getText();
        String curp = campoCurp.getText();
        String direccion = campoDireccion.getText();
        Date fechaNac = null;
        try {
            fechaNac = Date.from(campoFecha.getValue().atStartOfDay(ZoneId.systemDefault()).toInstant());
        } catch (Exception e) {
            System.err.println(e);
        }
        String localidad = campoLocalidad.getText();
        String materno = campoMaterno.getText();
        String nombre = campoNombre.getText();
        String paterno = campoPaterno.getText();
        String telefono = campoTelefono.getText();
        String usuario = campoUsuario.getText();
        String contrasena = campoContrasena.getText();
        String estado = "";
        try {
            estado = comboEstados.getSelectionModel().getSelectedItem().toString();
        } catch (Exception e) {
            System.err.println(e);
        }

        if ((cedula.isEmpty() || codiPostal.isEmpty() || curp.isEmpty() || direccion.isEmpty()
                || fechaNac == null || localidad.isEmpty() || nombre.isEmpty() || paterno.isEmpty())
                || telefono.isEmpty() || estado.isEmpty() || usuario.isEmpty()) {
            JOptionPane.showMessageDialog(null, "Campos vacios");
        } else {

            if (flag == 1) {
                if (!contrasena.isEmpty()) {

                    Medico m = new Medico();
                    m.setPersistencia(new MedicoInterfaz());
                    m.setUsuario(new Usuario(usuario, hashPassword(contrasena), 2, new UsuarioInterfaz()));
                    m.setApellidoMaterno(materno);
                    m.setApellidoPaterno(paterno);
                    m.setCedulaProfesional(Integer.parseInt(cedula));
                    m.setCodigoPostal(codiPostal);
                    m.setCurp(curp);
                    m.setDireccion(direccion);
                    m.setDisponibilidad(true);
                    m.setEstado(estado);
                    m.setFechaNacimiento(fechaNac);
                    m.setLocalidad(localidad);
                    m.setNombre(nombre);
                    m.setNumTelefono(telefono);
                    m.nuevoMedico();
                    accionInterfaz.actualizaTabla();
                    JOptionPane.showMessageDialog(null, "Médico registrado con éxito");
                    campoCedula.setText("");
                    campoCodigoPostal.setText("");
                    campoContrasena.setText("");
                    campoCurp.setText("");
                    campoDireccion.setText("");
                    campoFecha.setValue(null);
                    campoLocalidad.setText("");
                    campoMaterno.setText("");
                    campoNombre.setText("");
                    campoPaterno.setText("");
                    campoTelefono.setText("");
                    campoUsuario.setText("");
                } else {
                    JOptionPane.showMessageDialog(null, "Campos vacios");
                }

            }
            if (flag == 2) {

                medico.setPersistencia(new MedicoInterfaz());
                medico.setApellidoMaterno(materno);
                medico.setApellidoPaterno(paterno);
                medico.setCedulaProfesional(Integer.parseInt(cedula));
                medico.setCodigoPostal(codiPostal);
                medico.setCurp(curp);
                medico.setDireccion(direccion);
                medico.setDisponibilidad(true);
                medico.setEstado(estado);
                medico.setFechaNacimiento(fechaNac);
                medico.setLocalidad(localidad);
                medico.setNombre(nombre);
                medico.setNumTelefono(telefono);
                medico.editarMedico();
                accionInterfaz.actualizaTabla();
                JOptionPane.showMessageDialog(null, "Médico actualizado con éxito");
                Stage stageActualAdmi = (Stage) btnGuardar.getScene().getWindow();
                stageActualAdmi.close();

            }
        }
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        ObservableList<String> estados = FXCollections.observableArrayList(recursos.ListaEstados.getEstadosMexico());
        comboEstados.setItems(estados);
    }

    public void asignaParametros(AccionInterfaz accionInterfaz, int flag) {
        this.accionInterfaz = accionInterfaz;
        this.flag = flag;
    }

    public void asignaParametrosEditar(AccionInterfaz accionInterfaz, Medico medico, int flag) {
        this.accionInterfaz = accionInterfaz;
        this.medico = medico;
        this.flag = flag;

        campoCedula.setText(Integer.toString(medico.getCedulaProfesional()));
        campoCurp.setText(medico.getCurp());
        campoDireccion.setText(medico.getDireccion());
        LocalDate date = medico.getFechaNacimiento().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        campoFecha.setValue(date);
        campoCodigoPostal.setText(medico.getCodigoPostal());
        campoLocalidad.setText(medico.getLocalidad());
        campoMaterno.setText(medico.getApellidoMaterno());
        campoNombre.setText(medico.getNombre());
        campoPaterno.setText(medico.getApellidoPaterno());
        campoTelefono.setText(medico.getNumTelefono());
        campoUsuario.setText(medico.getUsuario().getUsuario());
        campoContrasena.setPromptText("Nueva contraseña (OPCIONAL)");
        campoUsuario.setDisable(true);
        campoContrasena.setDisable(true);

    }

    public static String hashPassword(String contrasena) throws NoSuchAlgorithmException,
            UnsupportedEncodingException {
        MessageDigest sha256 = MessageDigest.getInstance("SHA-256");
        sha256.update(contrasena.getBytes("UTF-8"));
        byte[] digest = sha256.digest();
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < digest.length; i++) {
            sb.append(String.format("%02x", digest[i]));
        }
        return sb.toString();
    }
}
