/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistencia.entidades;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author andres
 */
@Entity
@Table(name = "itinerario")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Itinerario.findAll", query = "SELECT i FROM Itinerario i")
    , @NamedQuery(name = "Itinerario.findByIdconsultorio", query = "SELECT i FROM Itinerario i WHERE i.itinerarioPK.idconsultorio = :idconsultorio")
    , @NamedQuery(name = "Itinerario.findByIdmedico", query = "SELECT i FROM Itinerario i WHERE i.itinerarioPK.idmedico = :idmedico")
    , @NamedQuery(name = "Itinerario.findByFecha", query = "SELECT i FROM Itinerario i WHERE i.fecha = :fecha")})
public class Itinerario implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected ItinerarioPK itinerarioPK;
    @Basic(optional = false)
    @Column(name = "fecha")
    @Temporal(TemporalType.DATE)
    private Date fecha;
    @JoinColumn(name = "idconsultorio", referencedColumnName = "idconsultorio", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Consultorio consultorio;
    @JoinColumn(name = "idmedico", referencedColumnName = "idmedico", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Medico medico;

    public Itinerario() {
    }

    public Itinerario(ItinerarioPK itinerarioPK) {
        this.itinerarioPK = itinerarioPK;
    }

    public Itinerario(ItinerarioPK itinerarioPK, Date fecha) {
        this.itinerarioPK = itinerarioPK;
        this.fecha = fecha;
    }

    public Itinerario(int idconsultorio, int idmedico) {
        this.itinerarioPK = new ItinerarioPK(idconsultorio, idmedico);
    }

    public ItinerarioPK getItinerarioPK() {
        return itinerarioPK;
    }

    public void setItinerarioPK(ItinerarioPK itinerarioPK) {
        this.itinerarioPK = itinerarioPK;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Consultorio getConsultorio() {
        return consultorio;
    }

    public void setConsultorio(Consultorio consultorio) {
        this.consultorio = consultorio;
    }

    public Medico getMedico() {
        return medico;
    }

    public void setMedico(Medico medico) {
        this.medico = medico;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (itinerarioPK != null ? itinerarioPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Itinerario)) {
            return false;
        }
        Itinerario other = (Itinerario) object;
        if ((this.itinerarioPK == null && other.itinerarioPK != null) || (this.itinerarioPK != null && !this.itinerarioPK.equals(other.itinerarioPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "persistencia.entidades.Itinerario[ itinerarioPK=" + itinerarioPK + " ]";
    }
    
}
