/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistencia.entidades;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author andres
 */
@Entity
@Table(name = "recepcionista")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Recepcionista.findAll", query = "SELECT r FROM Recepcionista r")
    , @NamedQuery(name = "Recepcionista.findByIdrecepcionista", query = "SELECT r FROM Recepcionista r WHERE r.idrecepcionista = :idrecepcionista")
    , @NamedQuery(name = "Recepcionista.findByNumPersonal", query = "SELECT r FROM Recepcionista r WHERE r.numPersonal = :numPersonal")
    , @NamedQuery(name = "Recepcionista.findByApellidoPaterno", query = "SELECT r FROM Recepcionista r WHERE r.apellidoPaterno = :apellidoPaterno")
    , @NamedQuery(name = "Recepcionista.findByApellidoMaterno", query = "SELECT r FROM Recepcionista r WHERE r.apellidoMaterno = :apellidoMaterno")
    , @NamedQuery(name = "Recepcionista.findByCodigoPostal", query = "SELECT r FROM Recepcionista r WHERE r.codigoPostal = :codigoPostal")
    , @NamedQuery(name = "Recepcionista.findByCurp", query = "SELECT r FROM Recepcionista r WHERE r.curp = :curp")
    , @NamedQuery(name = "Recepcionista.findByDireccion", query = "SELECT r FROM Recepcionista r WHERE r.direccion = :direccion")
    , @NamedQuery(name = "Recepcionista.findByEstado", query = "SELECT r FROM Recepcionista r WHERE r.estado = :estado")
    , @NamedQuery(name = "Recepcionista.findByFechaNacimiento", query = "SELECT r FROM Recepcionista r WHERE r.fechaNacimiento = :fechaNacimiento")
    , @NamedQuery(name = "Recepcionista.findByLocalidad", query = "SELECT r FROM Recepcionista r WHERE r.localidad = :localidad")
    , @NamedQuery(name = "Recepcionista.findByNombre", query = "SELECT r FROM Recepcionista r WHERE r.nombre = :nombre")
    , @NamedQuery(name = "Recepcionista.findByNumTelefono", query = "SELECT r FROM Recepcionista r WHERE r.numTelefono = :numTelefono")
    , @NamedQuery(name = "Recepcionista.findByIdusuario", query = "SELECT r FROM Recepcionista r WHERE r.idusuario = :idusuario")})
public class Recepcionista implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idrecepcionista")
    private Integer idrecepcionista;
    @Basic(optional = false)
    @Column(name = "numPersonal")
    private String numPersonal;
    @Basic(optional = false)
    @Column(name = "apellidoPaterno")
    private String apellidoPaterno;
    @Basic(optional = false)
    @Column(name = "apellidoMaterno")
    private String apellidoMaterno;
    @Basic(optional = false)
    @Column(name = "codigoPostal")
    private String codigoPostal;
    @Basic(optional = false)
    @Column(name = "curp")
    private String curp;
    @Basic(optional = false)
    @Column(name = "direccion")
    private String direccion;
    @Basic(optional = false)
    @Column(name = "estado")
    private String estado;
    @Basic(optional = false)
    @Column(name = "fechaNacimiento")
    @Temporal(TemporalType.DATE)
    private Date fechaNacimiento;
    @Basic(optional = false)
    @Column(name = "localidad")
    private String localidad;
    @Basic(optional = false)
    @Column(name = "nombre")
    private String nombre;
    @Basic(optional = false)
    @Column(name = "numTelefono")
    private String numTelefono;
    @Basic(optional = false)
    @Column(name = "idusuario")
    private int idusuario;
    @JoinColumn(name = "usuario", referencedColumnName = "usuario")
    @ManyToOne(optional = false)
    private Usuario usuario;

    public Recepcionista() {
    }

    public Recepcionista(Integer idrecepcionista) {
        this.idrecepcionista = idrecepcionista;
    }

    public Recepcionista(Integer idrecepcionista, String numPersonal, String apellidoPaterno, String apellidoMaterno, String codigoPostal, String curp, String direccion, String estado, Date fechaNacimiento, String localidad, String nombre, String numTelefono, int idusuario) {
        this.idrecepcionista = idrecepcionista;
        this.numPersonal = numPersonal;
        this.apellidoPaterno = apellidoPaterno;
        this.apellidoMaterno = apellidoMaterno;
        this.codigoPostal = codigoPostal;
        this.curp = curp;
        this.direccion = direccion;
        this.estado = estado;
        this.fechaNacimiento = fechaNacimiento;
        this.localidad = localidad;
        this.nombre = nombre;
        this.numTelefono = numTelefono;
        this.idusuario = idusuario;
    }

    public Integer getIdrecepcionista() {
        return idrecepcionista;
    }

    public void setIdrecepcionista(Integer idrecepcionista) {
        this.idrecepcionista = idrecepcionista;
    }

    public String getNumPersonal() {
        return numPersonal;
    }

    public void setNumPersonal(String numPersonal) {
        this.numPersonal = numPersonal;
    }

    public String getApellidoPaterno() {
        return apellidoPaterno;
    }

    public void setApellidoPaterno(String apellidoPaterno) {
        this.apellidoPaterno = apellidoPaterno;
    }

    public String getApellidoMaterno() {
        return apellidoMaterno;
    }

    public void setApellidoMaterno(String apellidoMaterno) {
        this.apellidoMaterno = apellidoMaterno;
    }

    public String getCodigoPostal() {
        return codigoPostal;
    }

    public void setCodigoPostal(String codigoPostal) {
        this.codigoPostal = codigoPostal;
    }

    public String getCurp() {
        return curp;
    }

    public void setCurp(String curp) {
        this.curp = curp;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public Date getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getLocalidad() {
        return localidad;
    }

    public void setLocalidad(String localidad) {
        this.localidad = localidad;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getNumTelefono() {
        return numTelefono;
    }

    public void setNumTelefono(String numTelefono) {
        this.numTelefono = numTelefono;
    }

    public int getIdusuario() {
        return idusuario;
    }

    public void setIdusuario(int idusuario) {
        this.idusuario = idusuario;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idrecepcionista != null ? idrecepcionista.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Recepcionista)) {
            return false;
        }
        Recepcionista other = (Recepcionista) object;
        if ((this.idrecepcionista == null && other.idrecepcionista != null) || (this.idrecepcionista != null && !this.idrecepcionista.equals(other.idrecepcionista))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "persistencia.entidades.Recepcionista[ idrecepcionista=" + idrecepcionista + " ]";
    }
    
}
