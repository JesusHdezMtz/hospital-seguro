/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistencia.entidades;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author andres
 */
@Entity
@Table(name = "consultorio")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Consultorio.findAll", query = "SELECT c FROM Consultorio c")
    , @NamedQuery(name = "Consultorio.findByIdconsultorio", query = "SELECT c FROM Consultorio c WHERE c.idconsultorio = :idconsultorio")
    , @NamedQuery(name = "Consultorio.findByNumero", query = "SELECT c FROM Consultorio c WHERE c.numero = :numero")})
public class Consultorio implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idconsultorio")
    private Integer idconsultorio;
    @Basic(optional = false)
    @Column(name = "numero")
    private int numero;
    @JoinColumn(name = "idcola", referencedColumnName = "idcola")
    @ManyToOne(optional = false)
    private Cola idcola;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "consultorio")
    private List<Itinerario> itinerarioList;

    public Consultorio() {
    }

    public Consultorio(Integer idconsultorio) {
        this.idconsultorio = idconsultorio;
    }

    public Consultorio(Integer idconsultorio, int numero) {
        this.idconsultorio = idconsultorio;
        this.numero = numero;
    }

    public Integer getIdconsultorio() {
        return idconsultorio;
    }

    public void setIdconsultorio(Integer idconsultorio) {
        this.idconsultorio = idconsultorio;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public Cola getIdcola() {
        return idcola;
    }

    public void setIdcola(Cola idcola) {
        this.idcola = idcola;
    }

    @XmlTransient
    public List<Itinerario> getItinerarioList() {
        return itinerarioList;
    }

    public void setItinerarioList(List<Itinerario> itinerarioList) {
        this.itinerarioList = itinerarioList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idconsultorio != null ? idconsultorio.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Consultorio)) {
            return false;
        }
        Consultorio other = (Consultorio) object;
        if ((this.idconsultorio == null && other.idconsultorio != null) || (this.idconsultorio != null && !this.idconsultorio.equals(other.idconsultorio))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "persistencia.entidades.Consultorio[ idconsultorio=" + idconsultorio + " ]";
    }
    
}
