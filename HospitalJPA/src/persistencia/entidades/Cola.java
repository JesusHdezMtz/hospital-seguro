/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistencia.entidades;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author andres
 */
@Entity
@Table(name = "cola")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Cola.findAll", query = "SELECT c FROM Cola c")
    , @NamedQuery(name = "Cola.findByIdcola", query = "SELECT c FROM Cola c WHERE c.idcola = :idcola")
    , @NamedQuery(name = "Cola.findByTamanio", query = "SELECT c FROM Cola c WHERE c.tamanio = :tamanio")})
public class Cola implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idcola")
    private Integer idcola;
    @Basic(optional = false)
    @Column(name = "tamanio")
    private int tamanio;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idcola")
    private List<Consultorio> consultorioList;
    @OneToMany(mappedBy = "idcola")
    private List<Paciente> pacienteList;

    public Cola() {
    }

    public Cola(Integer idcola) {
        this.idcola = idcola;
    }

    public Cola(Integer idcola, int tamanio) {
        this.idcola = idcola;
        this.tamanio = tamanio;
    }

    public Integer getIdcola() {
        return idcola;
    }

    public void setIdcola(Integer idcola) {
        this.idcola = idcola;
    }

    public int getTamanio() {
        return tamanio;
    }

    public void setTamanio(int tamanio) {
        this.tamanio = tamanio;
    }

    @XmlTransient
    public List<Consultorio> getConsultorioList() {
        return consultorioList;
    }

    public void setConsultorioList(List<Consultorio> consultorioList) {
        this.consultorioList = consultorioList;
    }

    @XmlTransient
    public List<Paciente> getPacienteList() {
        return pacienteList;
    }

    public void setPacienteList(List<Paciente> pacienteList) {
        this.pacienteList = pacienteList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idcola != null ? idcola.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Cola)) {
            return false;
        }
        Cola other = (Cola) object;
        if ((this.idcola == null && other.idcola != null) || (this.idcola != null && !this.idcola.equals(other.idcola))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "persistencia.entidades.Cola[ idcola=" + idcola + " ]";
    }
    
}
