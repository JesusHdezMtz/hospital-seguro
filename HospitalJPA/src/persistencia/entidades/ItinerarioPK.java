/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistencia.entidades;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author andres
 */
@Embeddable
public class ItinerarioPK implements Serializable {

    @Basic(optional = false)
    @Column(name = "idconsultorio")
    private int idconsultorio;
    @Basic(optional = false)
    @Column(name = "idmedico")
    private int idmedico;

    public ItinerarioPK() {
    }

    public ItinerarioPK(int idconsultorio, int idmedico) {
        this.idconsultorio = idconsultorio;
        this.idmedico = idmedico;
    }

    public int getIdconsultorio() {
        return idconsultorio;
    }

    public void setIdconsultorio(int idconsultorio) {
        this.idconsultorio = idconsultorio;
    }

    public int getIdmedico() {
        return idmedico;
    }

    public void setIdmedico(int idmedico) {
        this.idmedico = idmedico;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) idconsultorio;
        hash += (int) idmedico;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ItinerarioPK)) {
            return false;
        }
        ItinerarioPK other = (ItinerarioPK) object;
        if (this.idconsultorio != other.idconsultorio) {
            return false;
        }
        if (this.idmedico != other.idmedico) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "persistencia.entidades.ItinerarioPK[ idconsultorio=" + idconsultorio + ", idmedico=" + idmedico + " ]";
    }
    
}
