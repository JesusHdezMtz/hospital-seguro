/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistencia.controladores;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import persistencia.entidades.Usuario;
import persistencia.entidades.Itinerario;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import persistencia.controladores.exceptions.IllegalOrphanException;
import persistencia.controladores.exceptions.NonexistentEntityException;
import persistencia.entidades.Consulta;
import persistencia.entidades.Medico;

/**
 *
 * @author andres
 */
public class MedicoJpaController implements Serializable {

    public MedicoJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Medico medico) {
        if (medico.getItinerarioList() == null) {
            medico.setItinerarioList(new ArrayList<Itinerario>());
        }
        if (medico.getConsultaList() == null) {
            medico.setConsultaList(new ArrayList<Consulta>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Usuario usuario = medico.getUsuario();
            if (usuario != null) {
                usuario = em.getReference(usuario.getClass(), usuario.getUsuario());
                medico.setUsuario(usuario);
            }
            List<Itinerario> attachedItinerarioList = new ArrayList<Itinerario>();
            for (Itinerario itinerarioListItinerarioToAttach : medico.getItinerarioList()) {
                itinerarioListItinerarioToAttach = em.getReference(itinerarioListItinerarioToAttach.getClass(), itinerarioListItinerarioToAttach.getItinerarioPK());
                attachedItinerarioList.add(itinerarioListItinerarioToAttach);
            }
            medico.setItinerarioList(attachedItinerarioList);
            List<Consulta> attachedConsultaList = new ArrayList<Consulta>();
            for (Consulta consultaListConsultaToAttach : medico.getConsultaList()) {
                consultaListConsultaToAttach = em.getReference(consultaListConsultaToAttach.getClass(), consultaListConsultaToAttach.getIdconsulta());
                attachedConsultaList.add(consultaListConsultaToAttach);
            }
            medico.setConsultaList(attachedConsultaList);
            em.persist(medico);
            if (usuario != null) {
                usuario.getMedicoList().add(medico);
                usuario = em.merge(usuario);
            }
            for (Itinerario itinerarioListItinerario : medico.getItinerarioList()) {
                Medico oldMedicoOfItinerarioListItinerario = itinerarioListItinerario.getMedico();
                itinerarioListItinerario.setMedico(medico);
                itinerarioListItinerario = em.merge(itinerarioListItinerario);
                if (oldMedicoOfItinerarioListItinerario != null) {
                    oldMedicoOfItinerarioListItinerario.getItinerarioList().remove(itinerarioListItinerario);
                    oldMedicoOfItinerarioListItinerario = em.merge(oldMedicoOfItinerarioListItinerario);
                }
            }
            for (Consulta consultaListConsulta : medico.getConsultaList()) {
                Medico oldIdmedicoOfConsultaListConsulta = consultaListConsulta.getIdmedico();
                consultaListConsulta.setIdmedico(medico);
                consultaListConsulta = em.merge(consultaListConsulta);
                if (oldIdmedicoOfConsultaListConsulta != null) {
                    oldIdmedicoOfConsultaListConsulta.getConsultaList().remove(consultaListConsulta);
                    oldIdmedicoOfConsultaListConsulta = em.merge(oldIdmedicoOfConsultaListConsulta);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Medico medico) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Medico persistentMedico = em.find(Medico.class, medico.getIdmedico());
            Usuario usuarioOld = persistentMedico.getUsuario();
            Usuario usuarioNew = medico.getUsuario();
            List<Itinerario> itinerarioListOld = persistentMedico.getItinerarioList();
            List<Itinerario> itinerarioListNew = medico.getItinerarioList();
            List<Consulta> consultaListOld = persistentMedico.getConsultaList();
            List<Consulta> consultaListNew = medico.getConsultaList();
            List<String> illegalOrphanMessages = null;
            for (Itinerario itinerarioListOldItinerario : itinerarioListOld) {
                if (!itinerarioListNew.contains(itinerarioListOldItinerario)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Itinerario " + itinerarioListOldItinerario + " since its medico field is not nullable.");
                }
            }
            for (Consulta consultaListOldConsulta : consultaListOld) {
                if (!consultaListNew.contains(consultaListOldConsulta)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Consulta " + consultaListOldConsulta + " since its idmedico field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (usuarioNew != null) {
                usuarioNew = em.getReference(usuarioNew.getClass(), usuarioNew.getUsuario());
                medico.setUsuario(usuarioNew);
            }
            List<Itinerario> attachedItinerarioListNew = new ArrayList<Itinerario>();
            for (Itinerario itinerarioListNewItinerarioToAttach : itinerarioListNew) {
                itinerarioListNewItinerarioToAttach = em.getReference(itinerarioListNewItinerarioToAttach.getClass(), itinerarioListNewItinerarioToAttach.getItinerarioPK());
                attachedItinerarioListNew.add(itinerarioListNewItinerarioToAttach);
            }
            itinerarioListNew = attachedItinerarioListNew;
            medico.setItinerarioList(itinerarioListNew);
            List<Consulta> attachedConsultaListNew = new ArrayList<Consulta>();
            for (Consulta consultaListNewConsultaToAttach : consultaListNew) {
                consultaListNewConsultaToAttach = em.getReference(consultaListNewConsultaToAttach.getClass(), consultaListNewConsultaToAttach.getIdconsulta());
                attachedConsultaListNew.add(consultaListNewConsultaToAttach);
            }
            consultaListNew = attachedConsultaListNew;
            medico.setConsultaList(consultaListNew);
            medico = em.merge(medico);
            if (usuarioOld != null && !usuarioOld.equals(usuarioNew)) {
                usuarioOld.getMedicoList().remove(medico);
                usuarioOld = em.merge(usuarioOld);
            }
            if (usuarioNew != null && !usuarioNew.equals(usuarioOld)) {
                usuarioNew.getMedicoList().add(medico);
                usuarioNew = em.merge(usuarioNew);
            }
            for (Itinerario itinerarioListNewItinerario : itinerarioListNew) {
                if (!itinerarioListOld.contains(itinerarioListNewItinerario)) {
                    Medico oldMedicoOfItinerarioListNewItinerario = itinerarioListNewItinerario.getMedico();
                    itinerarioListNewItinerario.setMedico(medico);
                    itinerarioListNewItinerario = em.merge(itinerarioListNewItinerario);
                    if (oldMedicoOfItinerarioListNewItinerario != null && !oldMedicoOfItinerarioListNewItinerario.equals(medico)) {
                        oldMedicoOfItinerarioListNewItinerario.getItinerarioList().remove(itinerarioListNewItinerario);
                        oldMedicoOfItinerarioListNewItinerario = em.merge(oldMedicoOfItinerarioListNewItinerario);
                    }
                }
            }
            for (Consulta consultaListNewConsulta : consultaListNew) {
                if (!consultaListOld.contains(consultaListNewConsulta)) {
                    Medico oldIdmedicoOfConsultaListNewConsulta = consultaListNewConsulta.getIdmedico();
                    consultaListNewConsulta.setIdmedico(medico);
                    consultaListNewConsulta = em.merge(consultaListNewConsulta);
                    if (oldIdmedicoOfConsultaListNewConsulta != null && !oldIdmedicoOfConsultaListNewConsulta.equals(medico)) {
                        oldIdmedicoOfConsultaListNewConsulta.getConsultaList().remove(consultaListNewConsulta);
                        oldIdmedicoOfConsultaListNewConsulta = em.merge(oldIdmedicoOfConsultaListNewConsulta);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = medico.getIdmedico();
                if (findMedico(id) == null) {
                    throw new NonexistentEntityException("The medico with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Medico medico;
            try {
                medico = em.getReference(Medico.class, id);
                medico.getIdmedico();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The medico with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<Itinerario> itinerarioListOrphanCheck = medico.getItinerarioList();
            for (Itinerario itinerarioListOrphanCheckItinerario : itinerarioListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Medico (" + medico + ") cannot be destroyed since the Itinerario " + itinerarioListOrphanCheckItinerario + " in its itinerarioList field has a non-nullable medico field.");
            }
            List<Consulta> consultaListOrphanCheck = medico.getConsultaList();
            for (Consulta consultaListOrphanCheckConsulta : consultaListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Medico (" + medico + ") cannot be destroyed since the Consulta " + consultaListOrphanCheckConsulta + " in its consultaList field has a non-nullable idmedico field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Usuario usuario = medico.getUsuario();
            if (usuario != null) {
                usuario.getMedicoList().remove(medico);
                usuario = em.merge(usuario);
            }
            em.remove(medico);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Medico> findMedicoEntities() {
        return findMedicoEntities(true, -1, -1);
    }

    public List<Medico> findMedicoEntities(int maxResults, int firstResult) {
        return findMedicoEntities(false, maxResults, firstResult);
    }

    private List<Medico> findMedicoEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Medico.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Medico findMedico(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Medico.class, id);
        } finally {
            em.close();
        }
    }

    public int getMedicoCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Medico> rt = cq.from(Medico.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
