/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistencia.controladores;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import persistencia.entidades.Recepcionista;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import persistencia.controladores.exceptions.IllegalOrphanException;
import persistencia.controladores.exceptions.NonexistentEntityException;
import persistencia.controladores.exceptions.PreexistingEntityException;
import persistencia.entidades.Medico;
import persistencia.entidades.Usuario;

/**
 *
 * @author andres
 */
public class UsuarioJpaController implements Serializable {

    public UsuarioJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Usuario usuario) throws PreexistingEntityException, Exception {
        if (usuario.getRecepcionistaList() == null) {
            usuario.setRecepcionistaList(new ArrayList<Recepcionista>());
        }
        if (usuario.getMedicoList() == null) {
            usuario.setMedicoList(new ArrayList<Medico>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            List<Recepcionista> attachedRecepcionistaList = new ArrayList<Recepcionista>();
            for (Recepcionista recepcionistaListRecepcionistaToAttach : usuario.getRecepcionistaList()) {
                recepcionistaListRecepcionistaToAttach = em.getReference(recepcionistaListRecepcionistaToAttach.getClass(), recepcionistaListRecepcionistaToAttach.getIdrecepcionista());
                attachedRecepcionistaList.add(recepcionistaListRecepcionistaToAttach);
            }
            usuario.setRecepcionistaList(attachedRecepcionistaList);
            List<Medico> attachedMedicoList = new ArrayList<Medico>();
            for (Medico medicoListMedicoToAttach : usuario.getMedicoList()) {
                medicoListMedicoToAttach = em.getReference(medicoListMedicoToAttach.getClass(), medicoListMedicoToAttach.getIdmedico());
                attachedMedicoList.add(medicoListMedicoToAttach);
            }
            usuario.setMedicoList(attachedMedicoList);
            em.persist(usuario);
            for (Recepcionista recepcionistaListRecepcionista : usuario.getRecepcionistaList()) {
                Usuario oldUsuarioOfRecepcionistaListRecepcionista = recepcionistaListRecepcionista.getUsuario();
                recepcionistaListRecepcionista.setUsuario(usuario);
                recepcionistaListRecepcionista = em.merge(recepcionistaListRecepcionista);
                if (oldUsuarioOfRecepcionistaListRecepcionista != null) {
                    oldUsuarioOfRecepcionistaListRecepcionista.getRecepcionistaList().remove(recepcionistaListRecepcionista);
                    oldUsuarioOfRecepcionistaListRecepcionista = em.merge(oldUsuarioOfRecepcionistaListRecepcionista);
                }
            }
            for (Medico medicoListMedico : usuario.getMedicoList()) {
                Usuario oldUsuarioOfMedicoListMedico = medicoListMedico.getUsuario();
                medicoListMedico.setUsuario(usuario);
                medicoListMedico = em.merge(medicoListMedico);
                if (oldUsuarioOfMedicoListMedico != null) {
                    oldUsuarioOfMedicoListMedico.getMedicoList().remove(medicoListMedico);
                    oldUsuarioOfMedicoListMedico = em.merge(oldUsuarioOfMedicoListMedico);
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findUsuario(usuario.getUsuario()) != null) {
                throw new PreexistingEntityException("Usuario " + usuario + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Usuario usuario) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Usuario persistentUsuario = em.find(Usuario.class, usuario.getUsuario());
            List<Recepcionista> recepcionistaListOld = persistentUsuario.getRecepcionistaList();
            List<Recepcionista> recepcionistaListNew = usuario.getRecepcionistaList();
            List<Medico> medicoListOld = persistentUsuario.getMedicoList();
            List<Medico> medicoListNew = usuario.getMedicoList();
            List<String> illegalOrphanMessages = null;
            for (Recepcionista recepcionistaListOldRecepcionista : recepcionistaListOld) {
                if (!recepcionistaListNew.contains(recepcionistaListOldRecepcionista)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Recepcionista " + recepcionistaListOldRecepcionista + " since its usuario field is not nullable.");
                }
            }
            for (Medico medicoListOldMedico : medicoListOld) {
                if (!medicoListNew.contains(medicoListOldMedico)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Medico " + medicoListOldMedico + " since its usuario field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            List<Recepcionista> attachedRecepcionistaListNew = new ArrayList<Recepcionista>();
            for (Recepcionista recepcionistaListNewRecepcionistaToAttach : recepcionistaListNew) {
                recepcionistaListNewRecepcionistaToAttach = em.getReference(recepcionistaListNewRecepcionistaToAttach.getClass(), recepcionistaListNewRecepcionistaToAttach.getIdrecepcionista());
                attachedRecepcionistaListNew.add(recepcionistaListNewRecepcionistaToAttach);
            }
            recepcionistaListNew = attachedRecepcionistaListNew;
            usuario.setRecepcionistaList(recepcionistaListNew);
            List<Medico> attachedMedicoListNew = new ArrayList<Medico>();
            for (Medico medicoListNewMedicoToAttach : medicoListNew) {
                medicoListNewMedicoToAttach = em.getReference(medicoListNewMedicoToAttach.getClass(), medicoListNewMedicoToAttach.getIdmedico());
                attachedMedicoListNew.add(medicoListNewMedicoToAttach);
            }
            medicoListNew = attachedMedicoListNew;
            usuario.setMedicoList(medicoListNew);
            usuario = em.merge(usuario);
            for (Recepcionista recepcionistaListNewRecepcionista : recepcionistaListNew) {
                if (!recepcionistaListOld.contains(recepcionistaListNewRecepcionista)) {
                    Usuario oldUsuarioOfRecepcionistaListNewRecepcionista = recepcionistaListNewRecepcionista.getUsuario();
                    recepcionistaListNewRecepcionista.setUsuario(usuario);
                    recepcionistaListNewRecepcionista = em.merge(recepcionistaListNewRecepcionista);
                    if (oldUsuarioOfRecepcionistaListNewRecepcionista != null && !oldUsuarioOfRecepcionistaListNewRecepcionista.equals(usuario)) {
                        oldUsuarioOfRecepcionistaListNewRecepcionista.getRecepcionistaList().remove(recepcionistaListNewRecepcionista);
                        oldUsuarioOfRecepcionistaListNewRecepcionista = em.merge(oldUsuarioOfRecepcionistaListNewRecepcionista);
                    }
                }
            }
            for (Medico medicoListNewMedico : medicoListNew) {
                if (!medicoListOld.contains(medicoListNewMedico)) {
                    Usuario oldUsuarioOfMedicoListNewMedico = medicoListNewMedico.getUsuario();
                    medicoListNewMedico.setUsuario(usuario);
                    medicoListNewMedico = em.merge(medicoListNewMedico);
                    if (oldUsuarioOfMedicoListNewMedico != null && !oldUsuarioOfMedicoListNewMedico.equals(usuario)) {
                        oldUsuarioOfMedicoListNewMedico.getMedicoList().remove(medicoListNewMedico);
                        oldUsuarioOfMedicoListNewMedico = em.merge(oldUsuarioOfMedicoListNewMedico);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = usuario.getUsuario();
                if (findUsuario(id) == null) {
                    throw new NonexistentEntityException("The usuario with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Usuario usuario;
            try {
                usuario = em.getReference(Usuario.class, id);
                usuario.getUsuario();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The usuario with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<Recepcionista> recepcionistaListOrphanCheck = usuario.getRecepcionistaList();
            for (Recepcionista recepcionistaListOrphanCheckRecepcionista : recepcionistaListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Usuario (" + usuario + ") cannot be destroyed since the Recepcionista " + recepcionistaListOrphanCheckRecepcionista + " in its recepcionistaList field has a non-nullable usuario field.");
            }
            List<Medico> medicoListOrphanCheck = usuario.getMedicoList();
            for (Medico medicoListOrphanCheckMedico : medicoListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Usuario (" + usuario + ") cannot be destroyed since the Medico " + medicoListOrphanCheckMedico + " in its medicoList field has a non-nullable usuario field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            em.remove(usuario);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Usuario> findUsuarioEntities() {
        return findUsuarioEntities(true, -1, -1);
    }

    public List<Usuario> findUsuarioEntities(int maxResults, int firstResult) {
        return findUsuarioEntities(false, maxResults, firstResult);
    }

    private List<Usuario> findUsuarioEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Usuario.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Usuario findUsuario(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Usuario.class, id);
        } finally {
            em.close();
        }
    }

    public int getUsuarioCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Usuario> rt = cq.from(Usuario.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
