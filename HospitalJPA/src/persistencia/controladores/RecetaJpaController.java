/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistencia.controladores;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import persistencia.entidades.Medicamento;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import persistencia.controladores.exceptions.NonexistentEntityException;
import persistencia.entidades.Consulta;
import persistencia.entidades.Receta;

/**
 *
 * @author andres
 */
public class RecetaJpaController implements Serializable {

    public RecetaJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Receta receta) {
        if (receta.getMedicamentoList() == null) {
            receta.setMedicamentoList(new ArrayList<Medicamento>());
        }
        if (receta.getConsultaList() == null) {
            receta.setConsultaList(new ArrayList<Consulta>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            List<Medicamento> attachedMedicamentoList = new ArrayList<Medicamento>();
            for (Medicamento medicamentoListMedicamentoToAttach : receta.getMedicamentoList()) {
                medicamentoListMedicamentoToAttach = em.getReference(medicamentoListMedicamentoToAttach.getClass(), medicamentoListMedicamentoToAttach.getIdmedicamento());
                attachedMedicamentoList.add(medicamentoListMedicamentoToAttach);
            }
            receta.setMedicamentoList(attachedMedicamentoList);
            List<Consulta> attachedConsultaList = new ArrayList<Consulta>();
            for (Consulta consultaListConsultaToAttach : receta.getConsultaList()) {
                consultaListConsultaToAttach = em.getReference(consultaListConsultaToAttach.getClass(), consultaListConsultaToAttach.getIdconsulta());
                attachedConsultaList.add(consultaListConsultaToAttach);
            }
            receta.setConsultaList(attachedConsultaList);
            em.persist(receta);
            for (Medicamento medicamentoListMedicamento : receta.getMedicamentoList()) {
                medicamentoListMedicamento.getRecetaList().add(receta);
                medicamentoListMedicamento = em.merge(medicamentoListMedicamento);
            }
            for (Consulta consultaListConsulta : receta.getConsultaList()) {
                Receta oldRecetaIdrecetaOfConsultaListConsulta = consultaListConsulta.getRecetaIdreceta();
                consultaListConsulta.setRecetaIdreceta(receta);
                consultaListConsulta = em.merge(consultaListConsulta);
                if (oldRecetaIdrecetaOfConsultaListConsulta != null) {
                    oldRecetaIdrecetaOfConsultaListConsulta.getConsultaList().remove(consultaListConsulta);
                    oldRecetaIdrecetaOfConsultaListConsulta = em.merge(oldRecetaIdrecetaOfConsultaListConsulta);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Receta receta) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Receta persistentReceta = em.find(Receta.class, receta.getIdreceta());
            List<Medicamento> medicamentoListOld = persistentReceta.getMedicamentoList();
            List<Medicamento> medicamentoListNew = receta.getMedicamentoList();
            List<Consulta> consultaListOld = persistentReceta.getConsultaList();
            List<Consulta> consultaListNew = receta.getConsultaList();
            List<Medicamento> attachedMedicamentoListNew = new ArrayList<Medicamento>();
            for (Medicamento medicamentoListNewMedicamentoToAttach : medicamentoListNew) {
                medicamentoListNewMedicamentoToAttach = em.getReference(medicamentoListNewMedicamentoToAttach.getClass(), medicamentoListNewMedicamentoToAttach.getIdmedicamento());
                attachedMedicamentoListNew.add(medicamentoListNewMedicamentoToAttach);
            }
            medicamentoListNew = attachedMedicamentoListNew;
            receta.setMedicamentoList(medicamentoListNew);
            List<Consulta> attachedConsultaListNew = new ArrayList<Consulta>();
            for (Consulta consultaListNewConsultaToAttach : consultaListNew) {
                consultaListNewConsultaToAttach = em.getReference(consultaListNewConsultaToAttach.getClass(), consultaListNewConsultaToAttach.getIdconsulta());
                attachedConsultaListNew.add(consultaListNewConsultaToAttach);
            }
            consultaListNew = attachedConsultaListNew;
            receta.setConsultaList(consultaListNew);
            receta = em.merge(receta);
            for (Medicamento medicamentoListOldMedicamento : medicamentoListOld) {
                if (!medicamentoListNew.contains(medicamentoListOldMedicamento)) {
                    medicamentoListOldMedicamento.getRecetaList().remove(receta);
                    medicamentoListOldMedicamento = em.merge(medicamentoListOldMedicamento);
                }
            }
            for (Medicamento medicamentoListNewMedicamento : medicamentoListNew) {
                if (!medicamentoListOld.contains(medicamentoListNewMedicamento)) {
                    medicamentoListNewMedicamento.getRecetaList().add(receta);
                    medicamentoListNewMedicamento = em.merge(medicamentoListNewMedicamento);
                }
            }
            for (Consulta consultaListOldConsulta : consultaListOld) {
                if (!consultaListNew.contains(consultaListOldConsulta)) {
                    consultaListOldConsulta.setRecetaIdreceta(null);
                    consultaListOldConsulta = em.merge(consultaListOldConsulta);
                }
            }
            for (Consulta consultaListNewConsulta : consultaListNew) {
                if (!consultaListOld.contains(consultaListNewConsulta)) {
                    Receta oldRecetaIdrecetaOfConsultaListNewConsulta = consultaListNewConsulta.getRecetaIdreceta();
                    consultaListNewConsulta.setRecetaIdreceta(receta);
                    consultaListNewConsulta = em.merge(consultaListNewConsulta);
                    if (oldRecetaIdrecetaOfConsultaListNewConsulta != null && !oldRecetaIdrecetaOfConsultaListNewConsulta.equals(receta)) {
                        oldRecetaIdrecetaOfConsultaListNewConsulta.getConsultaList().remove(consultaListNewConsulta);
                        oldRecetaIdrecetaOfConsultaListNewConsulta = em.merge(oldRecetaIdrecetaOfConsultaListNewConsulta);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = receta.getIdreceta();
                if (findReceta(id) == null) {
                    throw new NonexistentEntityException("The receta with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Receta receta;
            try {
                receta = em.getReference(Receta.class, id);
                receta.getIdreceta();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The receta with id " + id + " no longer exists.", enfe);
            }
            List<Medicamento> medicamentoList = receta.getMedicamentoList();
            for (Medicamento medicamentoListMedicamento : medicamentoList) {
                medicamentoListMedicamento.getRecetaList().remove(receta);
                medicamentoListMedicamento = em.merge(medicamentoListMedicamento);
            }
            List<Consulta> consultaList = receta.getConsultaList();
            for (Consulta consultaListConsulta : consultaList) {
                consultaListConsulta.setRecetaIdreceta(null);
                consultaListConsulta = em.merge(consultaListConsulta);
            }
            em.remove(receta);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Receta> findRecetaEntities() {
        return findRecetaEntities(true, -1, -1);
    }

    public List<Receta> findRecetaEntities(int maxResults, int firstResult) {
        return findRecetaEntities(false, maxResults, firstResult);
    }

    private List<Receta> findRecetaEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Receta.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Receta findReceta(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Receta.class, id);
        } finally {
            em.close();
        }
    }

    public int getRecetaCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Receta> rt = cq.from(Receta.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
