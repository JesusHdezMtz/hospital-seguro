/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistencia.controladores;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import persistencia.entidades.Consultorio;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import persistencia.controladores.exceptions.IllegalOrphanException;
import persistencia.controladores.exceptions.NonexistentEntityException;
import persistencia.entidades.Cola;
import persistencia.entidades.Paciente;

/**
 *
 * @author Jahir
 */
public class ColaJpaController implements Serializable {

    
    public ColaJpaController() {
        this.emf = Persistence.createEntityManagerFactory("HospitalJPAPU");
    }
    
    public ColaJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Cola cola) {
        if (cola.getConsultorioList() == null) {
            cola.setConsultorioList(new ArrayList<Consultorio>());
        }
        if (cola.getPacienteList() == null) {
            cola.setPacienteList(new ArrayList<Paciente>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            List<Consultorio> attachedConsultorioList = new ArrayList<Consultorio>();
            for (Consultorio consultorioListConsultorioToAttach : cola.getConsultorioList()) {
                consultorioListConsultorioToAttach = em.getReference(consultorioListConsultorioToAttach.getClass(), consultorioListConsultorioToAttach.getIdconsultorio());
                attachedConsultorioList.add(consultorioListConsultorioToAttach);
            }
            cola.setConsultorioList(attachedConsultorioList);
            List<Paciente> attachedPacienteList = new ArrayList<Paciente>();
            for (Paciente pacienteListPacienteToAttach : cola.getPacienteList()) {
                pacienteListPacienteToAttach = em.getReference(pacienteListPacienteToAttach.getClass(), pacienteListPacienteToAttach.getIdpaciente());
                attachedPacienteList.add(pacienteListPacienteToAttach);
            }
            cola.setPacienteList(attachedPacienteList);
            em.persist(cola);
            for (Consultorio consultorioListConsultorio : cola.getConsultorioList()) {
                Cola oldIdcolaOfConsultorioListConsultorio = consultorioListConsultorio.getIdcola();
                consultorioListConsultorio.setIdcola(cola);
                consultorioListConsultorio = em.merge(consultorioListConsultorio);
                if (oldIdcolaOfConsultorioListConsultorio != null) {
                    oldIdcolaOfConsultorioListConsultorio.getConsultorioList().remove(consultorioListConsultorio);
                    oldIdcolaOfConsultorioListConsultorio = em.merge(oldIdcolaOfConsultorioListConsultorio);
                }
            }
            for (Paciente pacienteListPaciente : cola.getPacienteList()) {
                Cola oldIdcolaOfPacienteListPaciente = pacienteListPaciente.getIdcola();
                pacienteListPaciente.setIdcola(cola);
                pacienteListPaciente = em.merge(pacienteListPaciente);
                if (oldIdcolaOfPacienteListPaciente != null) {
                    oldIdcolaOfPacienteListPaciente.getPacienteList().remove(pacienteListPaciente);
                    oldIdcolaOfPacienteListPaciente = em.merge(oldIdcolaOfPacienteListPaciente);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Cola cola) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Cola persistentCola = em.find(Cola.class, cola.getIdcola());
            List<Consultorio> consultorioListOld = persistentCola.getConsultorioList();
            List<Consultorio> consultorioListNew = cola.getConsultorioList();
            List<Paciente> pacienteListOld = persistentCola.getPacienteList();
            List<Paciente> pacienteListNew = cola.getPacienteList();
            List<String> illegalOrphanMessages = null;
            for (Consultorio consultorioListOldConsultorio : consultorioListOld) {
                if (!consultorioListNew.contains(consultorioListOldConsultorio)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Consultorio " + consultorioListOldConsultorio + " since its idcola field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            List<Consultorio> attachedConsultorioListNew = new ArrayList<Consultorio>();
            for (Consultorio consultorioListNewConsultorioToAttach : consultorioListNew) {
                consultorioListNewConsultorioToAttach = em.getReference(consultorioListNewConsultorioToAttach.getClass(), consultorioListNewConsultorioToAttach.getIdconsultorio());
                attachedConsultorioListNew.add(consultorioListNewConsultorioToAttach);
            }
            consultorioListNew = attachedConsultorioListNew;
            cola.setConsultorioList(consultorioListNew);
            List<Paciente> attachedPacienteListNew = new ArrayList<Paciente>();
            for (Paciente pacienteListNewPacienteToAttach : pacienteListNew) {
                pacienteListNewPacienteToAttach = em.getReference(pacienteListNewPacienteToAttach.getClass(), pacienteListNewPacienteToAttach.getIdpaciente());
                attachedPacienteListNew.add(pacienteListNewPacienteToAttach);
            }
            pacienteListNew = attachedPacienteListNew;
            cola.setPacienteList(pacienteListNew);
            cola = em.merge(cola);
            for (Consultorio consultorioListNewConsultorio : consultorioListNew) {
                if (!consultorioListOld.contains(consultorioListNewConsultorio)) {
                    Cola oldIdcolaOfConsultorioListNewConsultorio = consultorioListNewConsultorio.getIdcola();
                    consultorioListNewConsultorio.setIdcola(cola);
                    consultorioListNewConsultorio = em.merge(consultorioListNewConsultorio);
                    if (oldIdcolaOfConsultorioListNewConsultorio != null && !oldIdcolaOfConsultorioListNewConsultorio.equals(cola)) {
                        oldIdcolaOfConsultorioListNewConsultorio.getConsultorioList().remove(consultorioListNewConsultorio);
                        oldIdcolaOfConsultorioListNewConsultorio = em.merge(oldIdcolaOfConsultorioListNewConsultorio);
                    }
                }
            }
            for (Paciente pacienteListOldPaciente : pacienteListOld) {
                if (!pacienteListNew.contains(pacienteListOldPaciente)) {
                    pacienteListOldPaciente.setIdcola(null);
                    pacienteListOldPaciente = em.merge(pacienteListOldPaciente);
                }
            }
            for (Paciente pacienteListNewPaciente : pacienteListNew) {
                if (!pacienteListOld.contains(pacienteListNewPaciente)) {
                    Cola oldIdcolaOfPacienteListNewPaciente = pacienteListNewPaciente.getIdcola();
                    pacienteListNewPaciente.setIdcola(cola);
                    pacienteListNewPaciente = em.merge(pacienteListNewPaciente);
                    if (oldIdcolaOfPacienteListNewPaciente != null && !oldIdcolaOfPacienteListNewPaciente.equals(cola)) {
                        oldIdcolaOfPacienteListNewPaciente.getPacienteList().remove(pacienteListNewPaciente);
                        oldIdcolaOfPacienteListNewPaciente = em.merge(oldIdcolaOfPacienteListNewPaciente);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = cola.getIdcola();
                if (findCola(id) == null) {
                    throw new NonexistentEntityException("The cola with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Cola cola;
            try {
                cola = em.getReference(Cola.class, id);
                cola.getIdcola();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The cola with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<Consultorio> consultorioListOrphanCheck = cola.getConsultorioList();
            for (Consultorio consultorioListOrphanCheckConsultorio : consultorioListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Cola (" + cola + ") cannot be destroyed since the Consultorio " + consultorioListOrphanCheckConsultorio + " in its consultorioList field has a non-nullable idcola field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            List<Paciente> pacienteList = cola.getPacienteList();
            for (Paciente pacienteListPaciente : pacienteList) {
                pacienteListPaciente.setIdcola(null);
                pacienteListPaciente = em.merge(pacienteListPaciente);
            }
            em.remove(cola);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Cola> findColaEntities() {
        return findColaEntities(true, -1, -1);
    }

    public List<Cola> findColaEntities(int maxResults, int firstResult) {
        return findColaEntities(false, maxResults, firstResult);
    }

    private List<Cola> findColaEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Cola.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Cola findCola(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Cola.class, id);
        } finally {
            em.close();
        }
    }

    public int getColaCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Cola> rt = cq.from(Cola.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
