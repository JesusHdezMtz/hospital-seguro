/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistencia.controladores;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import persistencia.entidades.Paciente;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import persistencia.controladores.exceptions.IllegalOrphanException;
import persistencia.controladores.exceptions.NonexistentEntityException;
import persistencia.entidades.Seguro;

/**
 *
 * @author andres
 */
public class SeguroJpaController implements Serializable {

    public SeguroJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Seguro seguro) {
        if (seguro.getPacienteList() == null) {
            seguro.setPacienteList(new ArrayList<Paciente>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            List<Paciente> attachedPacienteList = new ArrayList<Paciente>();
            for (Paciente pacienteListPacienteToAttach : seguro.getPacienteList()) {
                pacienteListPacienteToAttach = em.getReference(pacienteListPacienteToAttach.getClass(), pacienteListPacienteToAttach.getIdpaciente());
                attachedPacienteList.add(pacienteListPacienteToAttach);
            }
            seguro.setPacienteList(attachedPacienteList);
            em.persist(seguro);
            for (Paciente pacienteListPaciente : seguro.getPacienteList()) {
                Seguro oldIdseguroOfPacienteListPaciente = pacienteListPaciente.getIdseguro();
                pacienteListPaciente.setIdseguro(seguro);
                pacienteListPaciente = em.merge(pacienteListPaciente);
                if (oldIdseguroOfPacienteListPaciente != null) {
                    oldIdseguroOfPacienteListPaciente.getPacienteList().remove(pacienteListPaciente);
                    oldIdseguroOfPacienteListPaciente = em.merge(oldIdseguroOfPacienteListPaciente);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Seguro seguro) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Seguro persistentSeguro = em.find(Seguro.class, seguro.getIdseguro());
            List<Paciente> pacienteListOld = persistentSeguro.getPacienteList();
            List<Paciente> pacienteListNew = seguro.getPacienteList();
            List<String> illegalOrphanMessages = null;
            for (Paciente pacienteListOldPaciente : pacienteListOld) {
                if (!pacienteListNew.contains(pacienteListOldPaciente)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Paciente " + pacienteListOldPaciente + " since its idseguro field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            List<Paciente> attachedPacienteListNew = new ArrayList<Paciente>();
            for (Paciente pacienteListNewPacienteToAttach : pacienteListNew) {
                pacienteListNewPacienteToAttach = em.getReference(pacienteListNewPacienteToAttach.getClass(), pacienteListNewPacienteToAttach.getIdpaciente());
                attachedPacienteListNew.add(pacienteListNewPacienteToAttach);
            }
            pacienteListNew = attachedPacienteListNew;
            seguro.setPacienteList(pacienteListNew);
            seguro = em.merge(seguro);
            for (Paciente pacienteListNewPaciente : pacienteListNew) {
                if (!pacienteListOld.contains(pacienteListNewPaciente)) {
                    Seguro oldIdseguroOfPacienteListNewPaciente = pacienteListNewPaciente.getIdseguro();
                    pacienteListNewPaciente.setIdseguro(seguro);
                    pacienteListNewPaciente = em.merge(pacienteListNewPaciente);
                    if (oldIdseguroOfPacienteListNewPaciente != null && !oldIdseguroOfPacienteListNewPaciente.equals(seguro)) {
                        oldIdseguroOfPacienteListNewPaciente.getPacienteList().remove(pacienteListNewPaciente);
                        oldIdseguroOfPacienteListNewPaciente = em.merge(oldIdseguroOfPacienteListNewPaciente);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = seguro.getIdseguro();
                if (findSeguro(id) == null) {
                    throw new NonexistentEntityException("The seguro with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Seguro seguro;
            try {
                seguro = em.getReference(Seguro.class, id);
                seguro.getIdseguro();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The seguro with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<Paciente> pacienteListOrphanCheck = seguro.getPacienteList();
            for (Paciente pacienteListOrphanCheckPaciente : pacienteListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Seguro (" + seguro + ") cannot be destroyed since the Paciente " + pacienteListOrphanCheckPaciente + " in its pacienteList field has a non-nullable idseguro field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            em.remove(seguro);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Seguro> findSeguroEntities() {
        return findSeguroEntities(true, -1, -1);
    }

    public List<Seguro> findSeguroEntities(int maxResults, int firstResult) {
        return findSeguroEntities(false, maxResults, firstResult);
    }

    private List<Seguro> findSeguroEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Seguro.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Seguro findSeguro(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Seguro.class, id);
        } finally {
            em.close();
        }
    }

    public int getSeguroCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Seguro> rt = cq.from(Seguro.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
