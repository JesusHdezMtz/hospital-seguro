/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistencia.controladores;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import persistencia.entidades.Cola;
import persistencia.entidades.Seguro;
import persistencia.entidades.Consulta;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import persistencia.controladores.exceptions.IllegalOrphanException;
import persistencia.controladores.exceptions.NonexistentEntityException;
import persistencia.entidades.Paciente;

/**
 *
 * @author andres
 */
public class PacienteJpaController implements Serializable {

    public PacienteJpaController() {
        this.emf = Persistence.createEntityManagerFactory("HospitalJPAPU");
    }

    public PacienteJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Paciente paciente) {
        if (paciente.getConsultaList() == null) {
            paciente.setConsultaList(new ArrayList<Consulta>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Cola idcola = paciente.getIdcola();
            if (idcola != null) {
                idcola = em.getReference(idcola.getClass(), idcola.getIdcola());
                paciente.setIdcola(idcola);
            }
            Seguro idseguro = paciente.getIdseguro();
            if (idseguro != null) {
                idseguro = em.getReference(idseguro.getClass(), idseguro.getIdseguro());
                paciente.setIdseguro(idseguro);
            }
            List<Consulta> attachedConsultaList = new ArrayList<Consulta>();
            for (Consulta consultaListConsultaToAttach : paciente.getConsultaList()) {
                consultaListConsultaToAttach = em.getReference(consultaListConsultaToAttach.getClass(), consultaListConsultaToAttach.getIdconsulta());
                attachedConsultaList.add(consultaListConsultaToAttach);
            }
            paciente.setConsultaList(attachedConsultaList);
            em.persist(paciente);
            if (idcola != null) {
                idcola.getPacienteList().add(paciente);
                idcola = em.merge(idcola);
            }
            if (idseguro != null) {
                idseguro.getPacienteList().add(paciente);
                idseguro = em.merge(idseguro);
            }
            for (Consulta consultaListConsulta : paciente.getConsultaList()) {
                Paciente oldIdpacienteOfConsultaListConsulta = consultaListConsulta.getIdpaciente();
                consultaListConsulta.setIdpaciente(paciente);
                consultaListConsulta = em.merge(consultaListConsulta);
                if (oldIdpacienteOfConsultaListConsulta != null) {
                    oldIdpacienteOfConsultaListConsulta.getConsultaList().remove(consultaListConsulta);
                    oldIdpacienteOfConsultaListConsulta = em.merge(oldIdpacienteOfConsultaListConsulta);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Paciente paciente) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Paciente persistentPaciente = em.find(Paciente.class, paciente.getIdpaciente());
            Cola idcolaOld = persistentPaciente.getIdcola();
            Cola idcolaNew = paciente.getIdcola();
            Seguro idseguroOld = persistentPaciente.getIdseguro();
            Seguro idseguroNew = paciente.getIdseguro();
            List<Consulta> consultaListOld = persistentPaciente.getConsultaList();
            List<Consulta> consultaListNew = paciente.getConsultaList();
            List<String> illegalOrphanMessages = null;
            for (Consulta consultaListOldConsulta : consultaListOld) {
                if (!consultaListNew.contains(consultaListOldConsulta)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Consulta " + consultaListOldConsulta + " since its idpaciente field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (idcolaNew != null) {
                idcolaNew = em.getReference(idcolaNew.getClass(), idcolaNew.getIdcola());
                paciente.setIdcola(idcolaNew);
            }
            if (idseguroNew != null) {
                idseguroNew = em.getReference(idseguroNew.getClass(), idseguroNew.getIdseguro());
                paciente.setIdseguro(idseguroNew);
            }
            List<Consulta> attachedConsultaListNew = new ArrayList<Consulta>();
            for (Consulta consultaListNewConsultaToAttach : consultaListNew) {
                consultaListNewConsultaToAttach = em.getReference(consultaListNewConsultaToAttach.getClass(), consultaListNewConsultaToAttach.getIdconsulta());
                attachedConsultaListNew.add(consultaListNewConsultaToAttach);
            }
            consultaListNew = attachedConsultaListNew;
            paciente.setConsultaList(consultaListNew);
            paciente = em.merge(paciente);
            if (idcolaOld != null && !idcolaOld.equals(idcolaNew)) {
                idcolaOld.getPacienteList().remove(paciente);
                idcolaOld = em.merge(idcolaOld);
            }
            if (idcolaNew != null && !idcolaNew.equals(idcolaOld)) {
                idcolaNew.getPacienteList().add(paciente);
                idcolaNew = em.merge(idcolaNew);
            }
            if (idseguroOld != null && !idseguroOld.equals(idseguroNew)) {
                idseguroOld.getPacienteList().remove(paciente);
                idseguroOld = em.merge(idseguroOld);
            }
            if (idseguroNew != null && !idseguroNew.equals(idseguroOld)) {
                idseguroNew.getPacienteList().add(paciente);
                idseguroNew = em.merge(idseguroNew);
            }
            for (Consulta consultaListNewConsulta : consultaListNew) {
                if (!consultaListOld.contains(consultaListNewConsulta)) {
                    Paciente oldIdpacienteOfConsultaListNewConsulta = consultaListNewConsulta.getIdpaciente();
                    consultaListNewConsulta.setIdpaciente(paciente);
                    consultaListNewConsulta = em.merge(consultaListNewConsulta);
                    if (oldIdpacienteOfConsultaListNewConsulta != null && !oldIdpacienteOfConsultaListNewConsulta.equals(paciente)) {
                        oldIdpacienteOfConsultaListNewConsulta.getConsultaList().remove(consultaListNewConsulta);
                        oldIdpacienteOfConsultaListNewConsulta = em.merge(oldIdpacienteOfConsultaListNewConsulta);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = paciente.getIdpaciente();
                if (findPaciente(id) == null) {
                    throw new NonexistentEntityException("The paciente with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Paciente paciente;
            try {
                paciente = em.getReference(Paciente.class, id);
                paciente.getIdpaciente();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The paciente with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<Consulta> consultaListOrphanCheck = paciente.getConsultaList();
            for (Consulta consultaListOrphanCheckConsulta : consultaListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Paciente (" + paciente + ") cannot be destroyed since the Consulta " + consultaListOrphanCheckConsulta + " in its consultaList field has a non-nullable idpaciente field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Cola idcola = paciente.getIdcola();
            if (idcola != null) {
                idcola.getPacienteList().remove(paciente);
                idcola = em.merge(idcola);
            }
            Seguro idseguro = paciente.getIdseguro();
            if (idseguro != null) {
                idseguro.getPacienteList().remove(paciente);
                idseguro = em.merge(idseguro);
            }
            em.remove(paciente);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Paciente> findPacienteEntities() {
        return findPacienteEntities(true, -1, -1);
    }

    public List<Paciente> findPacienteEntities(int maxResults, int firstResult) {
        return findPacienteEntities(false, maxResults, firstResult);
    }

    private List<Paciente> findPacienteEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Paciente.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Paciente findPaciente(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Paciente.class, id);
        } finally {
            em.close();
        }
    }

    public int getPacienteCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Paciente> rt = cq.from(Paciente.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

}
