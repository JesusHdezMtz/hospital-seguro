/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistencia.controladores;

import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import persistencia.controladores.exceptions.NonexistentEntityException;
import persistencia.entidades.Consulta;
import persistencia.entidades.Medico;
import persistencia.entidades.Paciente;
import persistencia.entidades.Receta;

/**
 *
 * @author andres
 */
public class ConsultaJpaController implements Serializable {

    public ConsultaJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    
    public ConsultaJpaController() {
            this.emf = Persistence.createEntityManagerFactory("HospitalJPAPU");
    }
    
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Consulta consulta) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Medico idmedico = consulta.getIdmedico();
            if (idmedico != null) {
                idmedico = em.getReference(idmedico.getClass(), idmedico.getIdmedico());
                consulta.setIdmedico(idmedico);
            }
            Paciente idpaciente = consulta.getIdpaciente();
            if (idpaciente != null) {
                idpaciente = em.getReference(idpaciente.getClass(), idpaciente.getIdpaciente());
                consulta.setIdpaciente(idpaciente);
            }
            Receta recetaIdreceta = consulta.getRecetaIdreceta();
            if (recetaIdreceta != null) {
                recetaIdreceta = em.getReference(recetaIdreceta.getClass(), recetaIdreceta.getIdreceta());
                consulta.setRecetaIdreceta(recetaIdreceta);
            }
            em.persist(consulta);
            if (idmedico != null) {
                idmedico.getConsultaList().add(consulta);
                idmedico = em.merge(idmedico);
            }
            if (idpaciente != null) {
                idpaciente.getConsultaList().add(consulta);
                idpaciente = em.merge(idpaciente);
            }
            if (recetaIdreceta != null) {
                recetaIdreceta.getConsultaList().add(consulta);
                recetaIdreceta = em.merge(recetaIdreceta);
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Consulta consulta) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Consulta persistentConsulta = em.find(Consulta.class, consulta.getIdconsulta());
            Medico idmedicoOld = persistentConsulta.getIdmedico();
            Medico idmedicoNew = consulta.getIdmedico();
            Paciente idpacienteOld = persistentConsulta.getIdpaciente();
            Paciente idpacienteNew = consulta.getIdpaciente();
            Receta recetaIdrecetaOld = persistentConsulta.getRecetaIdreceta();
            Receta recetaIdrecetaNew = consulta.getRecetaIdreceta();
            if (idmedicoNew != null) {
                idmedicoNew = em.getReference(idmedicoNew.getClass(), idmedicoNew.getIdmedico());
                consulta.setIdmedico(idmedicoNew);
            }
            if (idpacienteNew != null) {
                idpacienteNew = em.getReference(idpacienteNew.getClass(), idpacienteNew.getIdpaciente());
                consulta.setIdpaciente(idpacienteNew);
            }
            if (recetaIdrecetaNew != null) {
                recetaIdrecetaNew = em.getReference(recetaIdrecetaNew.getClass(), recetaIdrecetaNew.getIdreceta());
                consulta.setRecetaIdreceta(recetaIdrecetaNew);
            }
            consulta = em.merge(consulta);
            if (idmedicoOld != null && !idmedicoOld.equals(idmedicoNew)) {
                idmedicoOld.getConsultaList().remove(consulta);
                idmedicoOld = em.merge(idmedicoOld);
            }
            if (idmedicoNew != null && !idmedicoNew.equals(idmedicoOld)) {
                idmedicoNew.getConsultaList().add(consulta);
                idmedicoNew = em.merge(idmedicoNew);
            }
            if (idpacienteOld != null && !idpacienteOld.equals(idpacienteNew)) {
                idpacienteOld.getConsultaList().remove(consulta);
                idpacienteOld = em.merge(idpacienteOld);
            }
            if (idpacienteNew != null && !idpacienteNew.equals(idpacienteOld)) {
                idpacienteNew.getConsultaList().add(consulta);
                idpacienteNew = em.merge(idpacienteNew);
            }
            if (recetaIdrecetaOld != null && !recetaIdrecetaOld.equals(recetaIdrecetaNew)) {
                recetaIdrecetaOld.getConsultaList().remove(consulta);
                recetaIdrecetaOld = em.merge(recetaIdrecetaOld);
            }
            if (recetaIdrecetaNew != null && !recetaIdrecetaNew.equals(recetaIdrecetaOld)) {
                recetaIdrecetaNew.getConsultaList().add(consulta);
                recetaIdrecetaNew = em.merge(recetaIdrecetaNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = consulta.getIdconsulta();
                if (findConsulta(id) == null) {
                    throw new NonexistentEntityException("The consulta with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Consulta consulta;
            try {
                consulta = em.getReference(Consulta.class, id);
                consulta.getIdconsulta();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The consulta with id " + id + " no longer exists.", enfe);
            }
            Medico idmedico = consulta.getIdmedico();
            if (idmedico != null) {
                idmedico.getConsultaList().remove(consulta);
                idmedico = em.merge(idmedico);
            }
            Paciente idpaciente = consulta.getIdpaciente();
            if (idpaciente != null) {
                idpaciente.getConsultaList().remove(consulta);
                idpaciente = em.merge(idpaciente);
            }
            Receta recetaIdreceta = consulta.getRecetaIdreceta();
            if (recetaIdreceta != null) {
                recetaIdreceta.getConsultaList().remove(consulta);
                recetaIdreceta = em.merge(recetaIdreceta);
            }
            em.remove(consulta);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Consulta> findConsultaEntities() {
        return findConsultaEntities(true, -1, -1);
    }

    public List<Consulta> findConsultaEntities(int maxResults, int firstResult) {
        return findConsultaEntities(false, maxResults, firstResult);
    }

    private List<Consulta> findConsultaEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Consulta.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Consulta findConsulta(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Consulta.class, id);
        } finally {
            em.close();
        }
    }

    public int getConsultaCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Consulta> rt = cq.from(Consulta.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
