/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistencia.controladores;

import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import persistencia.controladores.exceptions.NonexistentEntityException;
import persistencia.controladores.exceptions.PreexistingEntityException;
import persistencia.entidades.Consultorio;
import persistencia.entidades.Itinerario;
import persistencia.entidades.ItinerarioPK;
import persistencia.entidades.Medico;

/**
 *
 * @author andres
 */
public class ItinerarioJpaController implements Serializable {

    public ItinerarioJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Itinerario itinerario) throws PreexistingEntityException, Exception {
        if (itinerario.getItinerarioPK() == null) {
            itinerario.setItinerarioPK(new ItinerarioPK());
        }
        itinerario.getItinerarioPK().setIdmedico(itinerario.getMedico().getIdmedico());
        itinerario.getItinerarioPK().setIdconsultorio(itinerario.getConsultorio().getIdconsultorio());
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Consultorio consultorio = itinerario.getConsultorio();
            if (consultorio != null) {
                consultorio = em.getReference(consultorio.getClass(), consultorio.getIdconsultorio());
                itinerario.setConsultorio(consultorio);
            }
            Medico medico = itinerario.getMedico();
            if (medico != null) {
                medico = em.getReference(medico.getClass(), medico.getIdmedico());
                itinerario.setMedico(medico);
            }
            em.persist(itinerario);
            if (consultorio != null) {
                consultorio.getItinerarioList().add(itinerario);
                consultorio = em.merge(consultorio);
            }
            if (medico != null) {
                medico.getItinerarioList().add(itinerario);
                medico = em.merge(medico);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findItinerario(itinerario.getItinerarioPK()) != null) {
                throw new PreexistingEntityException("Itinerario " + itinerario + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Itinerario itinerario) throws NonexistentEntityException, Exception {
        itinerario.getItinerarioPK().setIdmedico(itinerario.getMedico().getIdmedico());
        itinerario.getItinerarioPK().setIdconsultorio(itinerario.getConsultorio().getIdconsultorio());
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Itinerario persistentItinerario = em.find(Itinerario.class, itinerario.getItinerarioPK());
            Consultorio consultorioOld = persistentItinerario.getConsultorio();
            Consultorio consultorioNew = itinerario.getConsultorio();
            Medico medicoOld = persistentItinerario.getMedico();
            Medico medicoNew = itinerario.getMedico();
            if (consultorioNew != null) {
                consultorioNew = em.getReference(consultorioNew.getClass(), consultorioNew.getIdconsultorio());
                itinerario.setConsultorio(consultorioNew);
            }
            if (medicoNew != null) {
                medicoNew = em.getReference(medicoNew.getClass(), medicoNew.getIdmedico());
                itinerario.setMedico(medicoNew);
            }
            itinerario = em.merge(itinerario);
            if (consultorioOld != null && !consultorioOld.equals(consultorioNew)) {
                consultorioOld.getItinerarioList().remove(itinerario);
                consultorioOld = em.merge(consultorioOld);
            }
            if (consultorioNew != null && !consultorioNew.equals(consultorioOld)) {
                consultorioNew.getItinerarioList().add(itinerario);
                consultorioNew = em.merge(consultorioNew);
            }
            if (medicoOld != null && !medicoOld.equals(medicoNew)) {
                medicoOld.getItinerarioList().remove(itinerario);
                medicoOld = em.merge(medicoOld);
            }
            if (medicoNew != null && !medicoNew.equals(medicoOld)) {
                medicoNew.getItinerarioList().add(itinerario);
                medicoNew = em.merge(medicoNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                ItinerarioPK id = itinerario.getItinerarioPK();
                if (findItinerario(id) == null) {
                    throw new NonexistentEntityException("The itinerario with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(ItinerarioPK id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Itinerario itinerario;
            try {
                itinerario = em.getReference(Itinerario.class, id);
                itinerario.getItinerarioPK();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The itinerario with id " + id + " no longer exists.", enfe);
            }
            Consultorio consultorio = itinerario.getConsultorio();
            if (consultorio != null) {
                consultorio.getItinerarioList().remove(itinerario);
                consultorio = em.merge(consultorio);
            }
            Medico medico = itinerario.getMedico();
            if (medico != null) {
                medico.getItinerarioList().remove(itinerario);
                medico = em.merge(medico);
            }
            em.remove(itinerario);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Itinerario> findItinerarioEntities() {
        return findItinerarioEntities(true, -1, -1);
    }

    public List<Itinerario> findItinerarioEntities(int maxResults, int firstResult) {
        return findItinerarioEntities(false, maxResults, firstResult);
    }

    private List<Itinerario> findItinerarioEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Itinerario.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Itinerario findItinerario(ItinerarioPK id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Itinerario.class, id);
        } finally {
            em.close();
        }
    }

    public int getItinerarioCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Itinerario> rt = cq.from(Itinerario.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
