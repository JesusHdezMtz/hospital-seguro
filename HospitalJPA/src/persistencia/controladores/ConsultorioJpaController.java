/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistencia.controladores;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import persistencia.entidades.Cola;
import persistencia.entidades.Itinerario;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import persistencia.controladores.exceptions.IllegalOrphanException;
import persistencia.controladores.exceptions.NonexistentEntityException;
import persistencia.entidades.Consultorio;

/**
 *
 * @author Jahir
 */
public class ConsultorioJpaController implements Serializable {

    public ConsultorioJpaController() {
        this.emf = Persistence.createEntityManagerFactory("HospitalJPAPU");
    }
    
    public ConsultorioJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }
    
    

    public void create(Consultorio consultorio) {
        if (consultorio.getItinerarioList() == null) {
            consultorio.setItinerarioList(new ArrayList<Itinerario>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Cola idcola = consultorio.getIdcola();
            if (idcola != null) {
                idcola = em.getReference(idcola.getClass(), idcola.getIdcola());
                consultorio.setIdcola(idcola);
            }
            List<Itinerario> attachedItinerarioList = new ArrayList<Itinerario>();
            for (Itinerario itinerarioListItinerarioToAttach : consultorio.getItinerarioList()) {
                itinerarioListItinerarioToAttach = em.getReference(itinerarioListItinerarioToAttach.getClass(), itinerarioListItinerarioToAttach.getItinerarioPK());
                attachedItinerarioList.add(itinerarioListItinerarioToAttach);
            }
            consultorio.setItinerarioList(attachedItinerarioList);
            em.persist(consultorio);
            if (idcola != null) {
                idcola.getConsultorioList().add(consultorio);
                idcola = em.merge(idcola);
            }
            for (Itinerario itinerarioListItinerario : consultorio.getItinerarioList()) {
                Consultorio oldConsultorioOfItinerarioListItinerario = itinerarioListItinerario.getConsultorio();
                itinerarioListItinerario.setConsultorio(consultorio);
                itinerarioListItinerario = em.merge(itinerarioListItinerario);
                if (oldConsultorioOfItinerarioListItinerario != null) {
                    oldConsultorioOfItinerarioListItinerario.getItinerarioList().remove(itinerarioListItinerario);
                    oldConsultorioOfItinerarioListItinerario = em.merge(oldConsultorioOfItinerarioListItinerario);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Consultorio consultorio) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Consultorio persistentConsultorio = em.find(Consultorio.class, consultorio.getIdconsultorio());
            Cola idcolaOld = persistentConsultorio.getIdcola();
            Cola idcolaNew = consultorio.getIdcola();
            List<Itinerario> itinerarioListOld = persistentConsultorio.getItinerarioList();
            List<Itinerario> itinerarioListNew = consultorio.getItinerarioList();
            List<String> illegalOrphanMessages = null;
            for (Itinerario itinerarioListOldItinerario : itinerarioListOld) {
                if (!itinerarioListNew.contains(itinerarioListOldItinerario)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Itinerario " + itinerarioListOldItinerario + " since its consultorio field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (idcolaNew != null) {
                idcolaNew = em.getReference(idcolaNew.getClass(), idcolaNew.getIdcola());
                consultorio.setIdcola(idcolaNew);
            }
            List<Itinerario> attachedItinerarioListNew = new ArrayList<Itinerario>();
            for (Itinerario itinerarioListNewItinerarioToAttach : itinerarioListNew) {
                itinerarioListNewItinerarioToAttach = em.getReference(itinerarioListNewItinerarioToAttach.getClass(), itinerarioListNewItinerarioToAttach.getItinerarioPK());
                attachedItinerarioListNew.add(itinerarioListNewItinerarioToAttach);
            }
            itinerarioListNew = attachedItinerarioListNew;
            consultorio.setItinerarioList(itinerarioListNew);
            consultorio = em.merge(consultorio);
            if (idcolaOld != null && !idcolaOld.equals(idcolaNew)) {
                idcolaOld.getConsultorioList().remove(consultorio);
                idcolaOld = em.merge(idcolaOld);
            }
            if (idcolaNew != null && !idcolaNew.equals(idcolaOld)) {
                idcolaNew.getConsultorioList().add(consultorio);
                idcolaNew = em.merge(idcolaNew);
            }
            for (Itinerario itinerarioListNewItinerario : itinerarioListNew) {
                if (!itinerarioListOld.contains(itinerarioListNewItinerario)) {
                    Consultorio oldConsultorioOfItinerarioListNewItinerario = itinerarioListNewItinerario.getConsultorio();
                    itinerarioListNewItinerario.setConsultorio(consultorio);
                    itinerarioListNewItinerario = em.merge(itinerarioListNewItinerario);
                    if (oldConsultorioOfItinerarioListNewItinerario != null && !oldConsultorioOfItinerarioListNewItinerario.equals(consultorio)) {
                        oldConsultorioOfItinerarioListNewItinerario.getItinerarioList().remove(itinerarioListNewItinerario);
                        oldConsultorioOfItinerarioListNewItinerario = em.merge(oldConsultorioOfItinerarioListNewItinerario);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = consultorio.getIdconsultorio();
                if (findConsultorio(id) == null) {
                    throw new NonexistentEntityException("The consultorio with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Consultorio consultorio;
            try {
                consultorio = em.getReference(Consultorio.class, id);
                consultorio.getIdconsultorio();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The consultorio with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<Itinerario> itinerarioListOrphanCheck = consultorio.getItinerarioList();
            for (Itinerario itinerarioListOrphanCheckItinerario : itinerarioListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Consultorio (" + consultorio + ") cannot be destroyed since the Itinerario " + itinerarioListOrphanCheckItinerario + " in its itinerarioList field has a non-nullable consultorio field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Cola idcola = consultorio.getIdcola();
            if (idcola != null) {
                idcola.getConsultorioList().remove(consultorio);
                idcola = em.merge(idcola);
            }
            em.remove(consultorio);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Consultorio> findConsultorioEntities() {
        return findConsultorioEntities(true, -1, -1);
    }

    public List<Consultorio> findConsultorioEntities(int maxResults, int firstResult) {
        return findConsultorioEntities(false, maxResults, firstResult);
    }

    private List<Consultorio> findConsultorioEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Consultorio.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Consultorio findConsultorio(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Consultorio.class, id);
        } finally {
            em.close();
        }
    }

    public int getConsultorioCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Consultorio> rt = cq.from(Consultorio.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
