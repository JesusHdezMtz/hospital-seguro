package persistencia.interfaces.clases;

import hospital.core.interfaces.IPersistencia;
import hospital.core.model.Seguro;
import hospitaljpa.utils.ClaseAEntidad;
import hospitaljpa.utils.EntidadAClase;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import persistencia.controladores.SeguroJpaController;
import persistencia.controladores.exceptions.IllegalOrphanException;
import persistencia.controladores.exceptions.NonexistentEntityException;

public class SeguroInterfaz implements IPersistencia<Seguro>{
    private final EntityManagerFactory emf = 
            Persistence.createEntityManagerFactory("HospitalJPAPU");
    SeguroJpaController jpa = new SeguroJpaController(emf);

    @Override
    public void nuevo(Seguro objeto) {
        jpa.create(ClaseAEntidad.seguroAEntidad(objeto));
    }

    @Override
    public void editar(Seguro objeto) {
        try {
            jpa.edit(ClaseAEntidad.seguroAEntidad(objeto));
        } catch (NonexistentEntityException ex) {
            System.err.println(ex);
        } catch (Exception ex) {
            System.err.println(ex);
        }
    }

    @Override
    public void eliminar(Integer id) {
        try {
            jpa.destroy(id);
        } catch (IllegalOrphanException | NonexistentEntityException ex) {
            System.err.println(ex);
        }
    }

    @Override
    public List<Seguro> todos() {
        List<Seguro> listaSeguros = new ArrayList<>();
        jpa.findSeguroEntities().forEach((seguro) -> {
            listaSeguros.add(EntidadAClase.entidadASeguro(seguro, this));
        });
        return listaSeguros;
    }

    @Override
    public int cantidad() {
        return jpa.getSeguroCount();
    }

    @Override
    public Seguro buscar(Integer id) {
        return EntidadAClase.entidadASeguro(jpa.findSeguro(id), this);
    }

    @Override
    public Seguro buscarPorNombre(String busqueda) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void eliminarPorNombre(String eliminar) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Seguro> recuperarPorId(Integer id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
