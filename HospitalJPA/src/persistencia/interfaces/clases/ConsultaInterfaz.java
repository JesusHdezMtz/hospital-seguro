package persistencia.interfaces.clases;

import hospital.core.interfaces.IPersistencia;
import hospital.core.model.Consulta;
import hospital.core.model.Paciente;
import hospitaljpa.utils.ClaseAEntidad;
import hospitaljpa.utils.EntidadAClase;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import persistencia.controladores.ConsultaJpaController;
import persistencia.controladores.exceptions.NonexistentEntityException;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author andres
 */
public class ConsultaInterfaz implements IPersistencia<hospital.core.model.Consulta>{

    private final EntityManagerFactory emf = 
            Persistence.createEntityManagerFactory("HospitalJPAPU");
    private final ConsultaJpaController jpa = new ConsultaJpaController(emf);
    
    @Override
    public void nuevo(Consulta objeto) {
        jpa.create(ClaseAEntidad.consultaAEntidad(objeto));
    }

    @Override
    public void editar(Consulta objeto) {
        try {
            jpa.edit(ClaseAEntidad.consultaAEntidad(objeto));
        } catch (NonexistentEntityException ex) {
            System.err.println(ex);
        } catch (Exception ex) {
            System.err.println(ex);
        }
    }

    @Override
    public void eliminar(Integer id) {
        try {
            jpa.destroy(id);
        } catch (NonexistentEntityException ex) {
            System.err.println(ex);
        }
    }

    @Override
    public List<Consulta> todos() {
        List<Consulta> listaConsulta = new ArrayList<>();
        jpa.findConsultaEntities().forEach((eConsulta) -> {
            listaConsulta.add(EntidadAClase.entidadAConsulta(eConsulta, this));
        });
        return listaConsulta;
    }

    @Override
    public int cantidad() {
        return jpa.getConsultaCount();
    }

    @Override
    public Consulta buscar(Integer id) {
        return EntidadAClase.entidadAConsulta(jpa.findConsulta(id), this);
    }

    @Override
    public Consulta buscarPorNombre(String busqueda) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void eliminarPorNombre(String eliminar) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Consulta> recuperarPorId(Integer id) {
        List<Consulta> listaConsultas = new ArrayList<>();
        jpa.findConsultaEntities().forEach((consulta)->{
            if(Objects.equals(consulta.getIdpaciente().getIdpaciente(), id)){
                listaConsultas.add(EntidadAClase.entidadAConsulta(consulta, this));
            }
        });
        
        return listaConsultas;
    }
    
}
