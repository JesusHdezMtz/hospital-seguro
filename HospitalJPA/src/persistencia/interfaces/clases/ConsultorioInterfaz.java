/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistencia.interfaces.clases;

import hospital.core.interfaces.IPersistencia;
import hospital.core.model.Consultorio;
import hospitaljpa.utils.ClaseAEntidad;
import hospitaljpa.utils.EntidadAClase;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import persistencia.controladores.ConsultorioJpaController;
import persistencia.controladores.exceptions.IllegalOrphanException;
import persistencia.controladores.exceptions.NonexistentEntityException;

/**
 *
 * @author andres
 */
public class ConsultorioInterfaz implements IPersistencia<hospital.core.model.Consultorio>{
    private final EntityManagerFactory emf = 
            Persistence.createEntityManagerFactory("HospitalJPAPU");
    private final ConsultorioJpaController jpa = new ConsultorioJpaController(emf);
    
    @Override
    public void nuevo(Consultorio objeto) {
        jpa.create(ClaseAEntidad.consultorioAEntidad(objeto));
    }

    @Override
    public void editar(Consultorio objeto) {
        try {
            jpa.edit(ClaseAEntidad.consultorioAEntidad(objeto));
        } catch (NonexistentEntityException ex) {
            System.err.println(ex);
        } catch (Exception ex) {
            System.err.println(ex);
        }
        
    }

    @Override
    public void eliminar(Integer id) {
        try {
            jpa.destroy(id);
        } catch (IllegalOrphanException | NonexistentEntityException ex) {
            System.err.println(ex);
        }
    }

    @Override
    public List<Consultorio> todos() {
        List<Consultorio> listaConsultorios = new ArrayList<>();
        jpa.findConsultorioEntities().forEach((consultorio) -> {
            listaConsultorios.add(EntidadAClase.entidadAConsultorio(consultorio, this));
        });
        return listaConsultorios;
    }

    @Override
    public int cantidad() {
        return jpa.getConsultorioCount();
    }

    @Override
    public Consultorio buscar(Integer id) {
        return EntidadAClase.entidadAConsultorio(jpa.findConsultorio(id), this);
    }

    @Override
    public Consultorio buscarPorNombre(String busqueda) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void eliminarPorNombre(String eliminar) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Consultorio> recuperarPorId(Integer id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
