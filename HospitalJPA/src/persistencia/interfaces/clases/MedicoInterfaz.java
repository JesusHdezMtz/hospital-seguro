/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistencia.interfaces.clases;

import hospital.core.interfaces.IPersistencia;
import hospital.core.model.Medico;
import hospitaljpa.utils.ClaseAEntidad;
import hospitaljpa.utils.EntidadAClase;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import persistencia.controladores.MedicoJpaController;
import persistencia.controladores.UsuarioJpaController;
import persistencia.controladores.exceptions.IllegalOrphanException;
import persistencia.controladores.exceptions.NonexistentEntityException;

/**
 *
 * @author andres
 */
public class MedicoInterfaz implements IPersistencia<Medico>{
    EntityManagerFactory emf = Persistence.createEntityManagerFactory("HospitalJPAPU");
    private final MedicoJpaController jpaDoctor = new MedicoJpaController(emf);
    private final UsuarioJpaController jpaUsuario = new UsuarioJpaController(emf);
    
    @Override
    public void nuevo(Medico objeto) {
        try{
            jpaUsuario.create(ClaseAEntidad.usuarioAEntidad(objeto.getUsuario()));
            jpaDoctor.create(ClaseAEntidad.medicoAEntidad(objeto));
        }catch (Exception ex){
            System.err.println(ex);
        }
    }

    @Override
    public void editar(Medico objeto) {
        persistencia.entidades.Medico medico = jpaDoctor.findMedico(objeto.getId());
        medico.setApellidoMaterno(objeto.getApellidoMaterno());
        medico.setApellidoPaterno(objeto.getApellidoPaterno());
        medico.setCedulaProfesional(objeto.getCedulaProfesional());
        medico.setCodigoPostal(objeto.getCodigoPostal());
        medico.setCurp(objeto.getCurp());
        medico.setDireccion(objeto.getDireccion());
        medico.setEstado(objeto.getEstado());
        medico.setFechaNacimiento(objeto.getFechaNacimiento());
        medico.setLocalidad(objeto.getLocalidad());
        medico.setNombre(objeto.getNombre());
        medico.setNumTelefono(objeto.getNumTelefono());
        medico.setUsuario(ClaseAEntidad.usuarioAEntidad(objeto.getUsuario()));
        try{
            jpaDoctor.edit(medico);
        }catch (Exception ex){
            System.err.println(ex);
        }

    }

    @Override
    public void eliminar(Integer id ) {
        try{
            jpaDoctor.destroy(id);
        }catch (IllegalOrphanException | NonexistentEntityException ex){
            System.err.println(ex);
        }
    }

    @Override
    public List<Medico> todos() {
        List<Medico> listaMedico = new ArrayList<>();
        jpaDoctor.findMedicoEntities().forEach((medico) -> {
            listaMedico.add(EntidadAClase.entidadDoctor(medico, this));
        });
        return listaMedico;
    }

    @Override
    public int cantidad() {
        return jpaDoctor.getMedicoCount();
    }

    @Override
    public Medico buscar(Integer id) {
        return EntidadAClase.entidadDoctor(jpaDoctor.findMedico(id), this);
    }

    @Override
    public Medico buscarPorNombre(String busqueda) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void eliminarPorNombre(String eliminar) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Medico> recuperarPorId(Integer id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
