/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistencia.interfaces.clases;

import hospital.core.interfaces.IPersistencia;
import hospital.core.model.Recepcionista;
import hospitaljpa.utils.ClaseAEntidad;
import hospitaljpa.utils.EntidadAClase;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import persistencia.controladores.RecepcionistaJpaController;
import persistencia.controladores.UsuarioJpaController;
import persistencia.controladores.exceptions.NonexistentEntityException;

/**
 *
 * @author andres
 */
public class RecepcionistaInterfaz implements IPersistencia<hospital.core.model.Recepcionista>{
    private final EntityManagerFactory emf = 
            Persistence.createEntityManagerFactory("HospitalJPAPU");
    private final RecepcionistaJpaController jpa = new RecepcionistaJpaController(emf);
        private final UsuarioJpaController jpaUsuario = new UsuarioJpaController(emf);

    
    @Override
    public void nuevo(Recepcionista objeto) {
        try{
            jpaUsuario.create(ClaseAEntidad.usuarioAEntidad(objeto.getUsuario()));
            jpa.create(ClaseAEntidad.recepcionistaAEntidad(objeto));
            
        }catch (Exception ex){
            System.err.println(ex);
        }
    }

    @Override
    public void editar(Recepcionista objecto) {
        try{
            jpa.edit(ClaseAEntidad.recepcionistaAEntidad(objecto));
        }catch (Exception ex){
            System.err.println(ex);
        }
    }

    @Override
    public void eliminar(Integer id) {
        try{
            jpa.destroy(id);
        }catch (NonexistentEntityException ex){
            System.err.println(ex);
        }
    }

    @Override
    public List<Recepcionista> todos() {
        List<Recepcionista> listaRecepcionista = new ArrayList<>();
        jpa.findRecepcionistaEntities().forEach((recepcionista) -> {
            listaRecepcionista.add(EntidadAClase.entidadARecepcionista(recepcionista, this));
        });
        return listaRecepcionista;
    }

    @Override
    public int cantidad() {
        return jpa.getRecepcionistaCount();
    }

    @Override
    public Recepcionista buscar(Integer id) {
        return EntidadAClase.entidadARecepcionista(
                jpa.findRecepcionista(id), this);
    }

    @Override
    public Recepcionista buscarPorNombre(String busqueda) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void eliminarPorNombre(String eliminar) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Recepcionista> recuperarPorId(Integer id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
