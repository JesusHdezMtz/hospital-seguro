/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistencia.interfaces.clases;

import hospital.core.interfaces.IPersistencia;
import hospital.core.model.*;
import hospitaljpa.utils.ClaseAEntidad;
import hospitaljpa.utils.EntidadAClase;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import persistencia.controladores.ColaJpaController;
import persistencia.controladores.exceptions.IllegalOrphanException;
import persistencia.controladores.exceptions.NonexistentEntityException;

/**
 *
 * @author andres
 */
public class ColaInterfaz implements IPersistencia<Cola>{
    
    private final EntityManagerFactory emf = 
            Persistence.createEntityManagerFactory("HospitalJPAPU");
    private final ColaJpaController jpa = new ColaJpaController(emf);

    @Override
    public void nuevo(Cola objeto) {
        try {
            jpa.create(ClaseAEntidad.colaAEntidad(objeto));
        } catch (Exception ex) {
            System.err.println(ex);
        }
    }

    @Override
    public void editar(Cola objecto) {
        try{
            jpa.edit(ClaseAEntidad.colaAEntidad(objecto));
        } catch (Exception ex){
            System.err.println(ex);
        }
    }

    @Override
    public void eliminar(Integer id) {
        try{
            jpa.destroy(id);
        } catch(IllegalOrphanException | NonexistentEntityException ex){
            System.err.println(ex);
        }
    }

    @Override
    public List<Cola> todos() {
        List<Cola> listaCola = new ArrayList<>();
        jpa.findColaEntities().forEach((cola) -> {
            listaCola.add(EntidadAClase.entidadACola(cola, this));
        });
        return listaCola;
    }

    @Override
    public int cantidad() {
        return jpa.getColaCount();
    }

    @Override
    public Cola buscar(Integer id) {
        return EntidadAClase.entidadACola(jpa.findCola(id), this);
    }

    @Override
    public Cola buscarPorNombre(String busqueda) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void eliminarPorNombre(String eliminar) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Cola> recuperarPorId(Integer id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
