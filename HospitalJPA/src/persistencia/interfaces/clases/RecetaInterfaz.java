/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistencia.interfaces.clases;

import hospital.core.interfaces.IPersistencia;
import hospital.core.model.Paciente;
import hospital.core.model.Receta;
import hospitaljpa.utils.ClaseAEntidad;
import hospitaljpa.utils.EntidadAClase;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import persistencia.controladores.RecetaJpaController;
import persistencia.controladores.exceptions.NonexistentEntityException;

/**
 *
 * @author andres
 */
public class RecetaInterfaz implements IPersistencia<Receta>{

    private final EntityManagerFactory emf = 
            Persistence.createEntityManagerFactory("HospitalJPAPU");
    private final RecetaJpaController jpa = new RecetaJpaController(emf);
    
    @Override
    public void nuevo(Receta objeto) {
        jpa.create(ClaseAEntidad.recetaAEntidad(objeto));
    }

    @Override
    public void editar(Receta objeto) {
        try {
            jpa.edit(ClaseAEntidad.recetaAEntidad(objeto));
        } catch (Exception ex) {
            System.err.println(ex);
        }
    }

    @Override
    public void eliminar(Integer id) {
        try {
            jpa.destroy(id);
        } catch (NonexistentEntityException ex) {
            System.err.println(ex);
        }
    }

    @Override
    public List<Receta> todos() {
        List<Receta> listaReceta = new ArrayList<>();
        jpa.findRecetaEntities().forEach((receta) -> {
            listaReceta.add(EntidadAClase.entidadAReceta(receta, this));
        });
        
        return listaReceta;
    }

    @Override
    public int cantidad() {
        return jpa.getRecetaCount();
    }

    @Override
    public Receta buscar(Integer id) {
        return EntidadAClase.entidadAReceta(jpa.findReceta(id), this);
    }

    @Override
    public Receta buscarPorNombre(String busqueda) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void eliminarPorNombre(String eliminar) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Receta> recuperarPorId(Integer id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
