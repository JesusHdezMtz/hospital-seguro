/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistencia.interfaces.clases;

import hospital.core.interfaces.IPersistencia;
import hospital.core.model.Medicamento;
import hospitaljpa.utils.ClaseAEntidad;
import hospitaljpa.utils.EntidadAClase;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.swing.JOptionPane;
import persistencia.controladores.MedicamentoJpaController;
import persistencia.controladores.exceptions.NonexistentEntityException;
import persistencia.entidades.Receta;

/**
 *
 * @author andres
 */
public class MedicamentoInterfaz implements IPersistencia<Medicamento> {

    EntityManagerFactory emf = Persistence.createEntityManagerFactory("HospitalJPAPU");
    private final MedicamentoJpaController jpa = new MedicamentoJpaController(emf);
    
    @Override
    public void nuevo(Medicamento medicamento) {
        persistencia.entidades.Medicamento medi = ClaseAEntidad.medicamentoAEntidad(medicamento);
        List<persistencia.entidades.Receta> recetas = new ArrayList<>();
        medi.setRecetaList(recetas);
        jpa.create(medi);
    }

    @Override
    public void editar(Medicamento t) {
        try {

            persistencia.entidades.Medicamento mediRec = jpa.findMedicamento(t.getId());
            mediRec.setNombre(t.getNombre());
            mediRec.setPresentacion(t.getPresentacion());
            mediRec.setComponenteActivo(t.getComponenteActivo());
            jpa.edit(mediRec);
        } catch (Exception ex) {
            System.err.println("MedicamentoInterfaz Editar" + ex);
        }

    }

    @Override
    public void eliminar(Integer intgr) {
        try {
            jpa.destroy(intgr);
        } catch (NonexistentEntityException ex) {
            System.err.println("MedicamentoInterfaz Eliminar" + ex);
        }
    }

    @Override
    public List<Medicamento> todos() {
        List<Medicamento> listaMedicamentos = new ArrayList<>();
        jpa.findMedicamentoEntities().forEach((medicamento) -> {
            listaMedicamentos.add(EntidadAClase.entidadAMedicamento(medicamento, this));
        });
        return listaMedicamentos;
    }

    @Override
    public int cantidad() {
        return jpa.getMedicamentoCount();
    }

    @Override
    public Medicamento buscar(Integer intgr) {
        return EntidadAClase.entidadAMedicamento(jpa.findMedicamento(intgr), this);
    }

    @Override
    public Medicamento buscarPorNombre(String busqueda) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void eliminarPorNombre(String eliminar) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Medicamento> recuperarPorId(Integer id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
