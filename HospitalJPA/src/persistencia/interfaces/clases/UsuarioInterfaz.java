package persistencia.interfaces.clases;

import hospital.core.interfaces.IPersistencia;
import hospital.core.model.Usuario;
import hospitaljpa.utils.ClaseAEntidad;
import hospitaljpa.utils.EntidadAClase;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import persistencia.controladores.UsuarioJpaController;
import persistencia.controladores.exceptions.IllegalOrphanException;
import persistencia.controladores.exceptions.NonexistentEntityException;

/**
 *
 * @author Jesús
 */
public class UsuarioInterfaz implements IPersistencia<Usuario>{
    
    private final EntityManagerFactory emf = 
            Persistence.createEntityManagerFactory("HospitalJPAPU");
    UsuarioJpaController jpa = new UsuarioJpaController(emf);

    @Override
    public void nuevo(Usuario objeto) {
        
        try{
            jpa.create(ClaseAEntidad.usuarioAEntidad(objeto));
            
        }catch (Exception ex){
            System.err.println(ex);
        }
    }

    @Override
    public void editar(Usuario objeto) {
        try{
            jpa.edit(ClaseAEntidad.usuarioAEntidad(objeto));
        }catch (Exception ex){
            System.err.println(ex);
        }
    }

    @Override
    public void eliminar(Integer id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.

    }

    @Override
    public List<Usuario> todos() {
         List<Usuario> listaUsuarios = new ArrayList<>();
        jpa.findUsuarioEntities().forEach((eUsuario) -> {
            listaUsuarios.add(EntidadAClase.entidadAUsuario(eUsuario, this));
        });
        return listaUsuarios;
    }

    @Override
    public int cantidad() {
        return jpa.getUsuarioCount();
    }

    @Override
    public Usuario buscar(Integer id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Usuario buscarPorNombre(String busqueda) {
        return EntidadAClase.entidadAUsuario(jpa.findUsuario(busqueda), this);
    }

    @Override
    public void eliminarPorNombre(String eliminar) {
        try {
            jpa.destroy(eliminar);
        } catch (IllegalOrphanException | NonexistentEntityException ex) {
            Logger.getLogger(UsuarioInterfaz.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public List<Usuario> recuperarPorId(Integer id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
