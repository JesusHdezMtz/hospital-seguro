/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistencia.interfaces.clases;

import hospital.core.interfaces.IPersistencia;
import hospital.core.model.Paciente;
import hospitaljpa.utils.ClaseAEntidad;
import hospitaljpa.utils.EntidadAClase;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import persistencia.controladores.PacienteJpaController;
import persistencia.controladores.exceptions.IllegalOrphanException;
import persistencia.controladores.exceptions.NonexistentEntityException;

/**
 *
 * @author andres
 */
public class PacienteInterfaz implements IPersistencia<hospital.core.model.Paciente>{
    EntityManagerFactory emf = Persistence.createEntityManagerFactory("HospitalJPAPU");
    private final PacienteJpaController jpa = new PacienteJpaController(emf);
    
    @Override
    public void nuevo(Paciente objeto) {
        try{
            jpa.create(ClaseAEntidad.pacienteAEntidad(objeto));
        }catch (Exception ex){
            System.err.println(ex);
        }
    }

    @Override
    public void editar(Paciente objecto) {
        try{
            jpa.edit(ClaseAEntidad.pacienteAEntidad(objecto));
        }catch (Exception ex){
            System.err.println(ex);
        }
    }

    @Override
    public void eliminar(Integer id) {
        try{
            jpa.destroy(id);
        }catch (IllegalOrphanException | NonexistentEntityException ex){
            System.err.println(ex);
        }
    }

    @Override
    public List<Paciente> todos() {
        List<Paciente> listaPaciente = new ArrayList<>();
        jpa.findPacienteEntities().forEach((paciente) -> {
            listaPaciente.add(EntidadAClase.entidadAPaciente(paciente, this));
        });
        return listaPaciente;
    }

    @Override
    public int cantidad() {
        return jpa.getPacienteCount();
    }

    @Override
    public Paciente buscar(Integer id) {
        return EntidadAClase.entidadAPaciente(jpa.findPaciente(id), this);
    }   

    @Override
    public void eliminarPorNombre(String eliminar) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Paciente buscarPorNombre(String busqueda) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<Paciente> recuperarPorId(Integer id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
