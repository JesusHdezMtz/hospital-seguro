/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hospitaljpa.utils;

import hospital.core.interfaces.IPersistencia;
import hospital.core.model.*;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author andres
 */
public abstract class EntidadAClase {
    
    public static Paciente entidadAPaciente (
            persistencia.entidades.Paciente ePaciente, 
            IPersistencia persistencia) {
        Paciente paciente = new Paciente();
        paciente.setApellidoMaterno(ePaciente.getApellidoMaterno());
        paciente.setApellidoPaterno(ePaciente.getApellidoPaterno());
        paciente.setCodigoPostal(ePaciente.getCodigoPostal());
        paciente.setCurp(ePaciente.getCurp());
        paciente.setDireccion(ePaciente.getDireccion());
        paciente.setEstado(ePaciente.getEstado());
        paciente.setFechaNacimiento(ePaciente.getFechaNacimiento());
        paciente.setLocalidad(ePaciente.getLocalidad());
        paciente.setNombre(ePaciente.getNombre());
        paciente.setNumTelefono(ePaciente.getNumTelefono());
        paciente.setNumeroSeguro(ePaciente.getNumeroSeguro());
        paciente.setPersistencia(persistencia);
        paciente.setSeguro(entidadASeguro(ePaciente.getIdseguro(), persistencia));
        paciente.setId(ePaciente.getIdpaciente());        
        
        return paciente;
    }
    
    public static Seguro entidadASeguro(persistencia.entidades.Seguro eSeguro, 
            IPersistencia persistencia){
        Seguro seguro = new Seguro(persistencia);
        seguro.setNombre(eSeguro.getNombre());
        seguro.setId(eSeguro.getIdseguro());
        return seguro;
    } 
            
    /*public static Cola entidadACola(persistencia.entidades.Cola eCola, 
            IPersistencia persistencia){
        List<Paciente> listaPacientes = new ArrayList();
        for(persistencia.entidades.Paciente paciente :  eCola.getPacienteList()){
            listaPacientes.add(entidadAPaciente(paciente, persistencia));
        }
        return new Cola(persistencia, listaPacientes);
    }*/
    
    public static Cola entidadACola(persistencia.entidades.Cola eCola, 
            IPersistencia persistencia){
        List<Paciente> listaPacientes = new ArrayList();
        for(persistencia.entidades.Paciente paciente :  eCola.getPacienteList()){
            listaPacientes.add(entidadAPaciente(paciente, persistencia));
        }
        return new Cola(persistencia, listaPacientes);
    }
    
    public static Medico entidadDoctor(persistencia.entidades.Medico eMedico, 
            IPersistencia persistencia){
        Medico medico = new Medico();
        medico.setId(eMedico.getIdmedico());
        medico.setUsuario(EntidadAClase.entidadAUsuario(eMedico.getUsuario(), persistencia));
        medico.setApellidoMaterno(eMedico.getApellidoMaterno());
        medico.setApellidoPaterno(eMedico.getApellidoPaterno());
        medico.setCedulaProfesional(eMedico.getCedulaProfesional());
        medico.setCodigoPostal(eMedico.getCodigoPostal());
        medico.setCurp(eMedico.getCurp());
        medico.setDireccion(eMedico.getDireccion());
        medico.setEstado(eMedico.getEstado());
        medico.setFechaNacimiento(eMedico.getFechaNacimiento());
        medico.setLocalidad(eMedico.getLocalidad());
        medico.setNombre(eMedico.getNombre());
        medico.setNumTelefono(eMedico.getNumTelefono());
        medico.setPersistencia(persistencia);
        
        return medico;
    }
    
    public static Recepcionista entidadARecepcionista(persistencia.entidades.Recepcionista
            eRecepcionista, IPersistencia persistencia){
        Recepcionista recepcionista = new Recepcionista();
        recepcionista.setUsuario(EntidadAClase.entidadAUsuario(eRecepcionista.getUsuario(), persistencia));
        recepcionista.setId(eRecepcionista.getIdrecepcionista());
        recepcionista.setApellidoMaterno(eRecepcionista.getApellidoMaterno());
        recepcionista.setApellidoPaterno(eRecepcionista.getApellidoPaterno());
        recepcionista.setNumPersonal(Integer.parseInt(eRecepcionista.getNumPersonal()));
        recepcionista.setCodigoPostal(eRecepcionista.getCodigoPostal());
        recepcionista.setCurp(eRecepcionista.getCurp());
        recepcionista.setDireccion(eRecepcionista.getDireccion());
        recepcionista.setEstado(eRecepcionista.getEstado());
        recepcionista.setFechaNacimiento(eRecepcionista.getFechaNacimiento());
        recepcionista.setLocalidad(eRecepcionista.getLocalidad());
        recepcionista.setNombre(eRecepcionista.getNombre());
        recepcionista.setNumTelefono(eRecepcionista.getNumTelefono());
        recepcionista.setPersistencia(persistencia);
        recepcionista.setId(eRecepcionista.getIdrecepcionista());
        
        return recepcionista;
    }
    
    public static Consulta entidadAConsulta(persistencia.entidades.Consulta
            eConsulta, IPersistencia persistencia){
        return new Consulta(eConsulta.getFechaYHora(), 
                entidadDoctor(eConsulta.getIdmedico(), persistencia), 
                entidadAPaciente(eConsulta.getIdpaciente(), persistencia), 
                eConsulta.getIdconsulta(), persistencia);
    }
    
    public static Consultorio entidadAConsultorio(
            persistencia.entidades.Consultorio eConsultorio, 
            IPersistencia persistencia){
        Consultorio consultorio = new Consultorio(eConsultorio.getNumero(), persistencia);
        consultorio.setCola(entidadACola(eConsultorio.getIdcola(), persistencia));
        consultorio.setId(eConsultorio.getIdconsultorio());
        
        return consultorio;
    }
    
    public static Usuario entidadAUsuario(persistencia.entidades.Usuario eUsuario, 
            IPersistencia persistencia){
        if(eUsuario != null){
            Usuario usuario = new Usuario(persistencia);
            usuario.setUsuario(eUsuario.getUsuario());
            usuario.setContrasena(eUsuario.getContrasena());
            usuario.setTipoAcceso(eUsuario.getTipoAcceso());
            return usuario;
        }
        return null;
    }
    public static Medicamento entidadAMedicamento(
            persistencia.entidades.Medicamento eMedicamento, 
            IPersistencia persistencia){
        Medicamento medicamento = new Medicamento(persistencia);
        medicamento.setComponenteActivo(eMedicamento.getComponenteActivo());
        medicamento.setId(eMedicamento.getIdmedicamento());
        medicamento.setNombre(eMedicamento.getNombre());
        medicamento.setPresentacion(eMedicamento.getPresentacion());
        
        return medicamento;
    }
    
    public static Receta entidadAReceta(
            persistencia.entidades.Receta eReceta, 
            IPersistencia persistencia){
        Receta receta = new Receta(persistencia);
        List<Medicamento> listaMedicamentos = new ArrayList<>();
        eReceta.getMedicamentoList().forEach((medicamento) -> {
            listaMedicamentos.add(entidadAMedicamento(medicamento, persistencia));
        });
        receta.setEstatura(eReceta.getEstatura());
        receta.setId(eReceta.getIdreceta());
        receta.setIndicacion(eReceta.getIndicacion());
        receta.setMedicamentos(listaMedicamentos);
        receta.setPeso(eReceta.getPeso());
        receta.setPresion(eReceta.getPresion());
        
        return receta;
    }
}
