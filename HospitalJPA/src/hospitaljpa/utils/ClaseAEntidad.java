/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hospitaljpa.utils;

import java.util.ArrayList;
import java.util.List;
import persistencia.entidades.*;

/**
 *
 * @author andres
 */
public abstract class ClaseAEntidad {
    
    public static Seguro seguroAEntidad(hospital.core.model.Seguro seguro){
        Seguro eSeguro = new Seguro();
        eSeguro.setNombre(seguro.getNombre());
        eSeguro.setIdseguro(seguro.getId());
        
        return eSeguro;
    }
    
    public static Usuario usuarioAEntidad(hospital.core.model.Usuario usuario){
        Usuario eUsuario = new Usuario();
        eUsuario.setUsuario(usuario.getUsuario());
        eUsuario.setContrasena(usuario.getContrasena());
        eUsuario.setTipoAcceso(usuario.getTipoAcceso());
        
        return eUsuario;
    }
    
    public static Paciente pacienteAEntidad(hospital.core.model.Paciente paciente) {
        Paciente ePaciente = new Paciente();
        ePaciente.setNumeroSeguro(paciente.getNumeroSeguro());
        ePaciente.setIdseguro(seguroAEntidad(paciente.getSeguro()));
        ePaciente.setIdpaciente(paciente.getId());
        ePaciente.setApellidoMaterno(paciente.getApellidoMaterno());
        ePaciente.setApellidoPaterno(paciente.getApellidoPaterno());
        ePaciente.setCodigoPostal(paciente.getCodigoPostal());
        ePaciente.setCurp(paciente.getCurp());
        ePaciente.setDireccion(paciente.getDireccion());
        ePaciente.setEstado(paciente.getEstado());
        ePaciente.setFechaNacimiento(paciente.getFechaNacimiento());
        ePaciente.setLocalidad(paciente.getLocalidad());
        ePaciente.setNombre(paciente.getNombre());
        ePaciente.setNumTelefono(paciente.getNumTelefono());
        //ePaciente.setIdpaciente(paciente.getId());
        
        return ePaciente;
    }
    
    public static Medico medicoAEntidad(hospital.core.model.Medico medico){
        hospital.core.model.Persona persona = medico;
        Medico eMedico = new Medico();
        eMedico.setUsuario(ClaseAEntidad.usuarioAEntidad(medico.getUsuario()));
        eMedico.setIdmedico(medico.getId());
        eMedico.setCedulaProfesional(medico.getCedulaProfesional());
        eMedico.setDisponibilidad(medico.getDisponibilidad() ? (short) 1 : (short)0);
        eMedico.setApellidoMaterno(medico.getApellidoMaterno());
        eMedico.setApellidoPaterno(medico.getApellidoPaterno());
        eMedico.setCodigoPostal(medico.getCodigoPostal());
        eMedico.setCurp(medico.getCurp());
        eMedico.setDireccion(medico.getDireccion());
        eMedico.setEstado(medico.getEstado());
        eMedico.setFechaNacimiento(medico.getFechaNacimiento());
        eMedico.setLocalidad(medico.getLocalidad());
        eMedico.setNombre(medico.getNombre());
        eMedico.setIdmedico(medico.getId());
        eMedico.setNumTelefono(medico.getNumTelefono());
        
        return eMedico;
    }
    
    public static Recepcionista recepcionistaAEntidad(hospital.core.model.Recepcionista recepcionista){
        hospital.core.model.Persona persona = recepcionista;
        Recepcionista eRecepcionista = new Recepcionista();
        eRecepcionista.setIdrecepcionista(recepcionista.getId());
        eRecepcionista.setUsuario(ClaseAEntidad.usuarioAEntidad(recepcionista.getUsuario()));
        eRecepcionista.setNumPersonal(Integer.toString(recepcionista.getNumPersonal()));
        eRecepcionista.setIdrecepcionista(recepcionista.getId());
        eRecepcionista.setApellidoMaterno(recepcionista.getApellidoMaterno());
        eRecepcionista.setApellidoPaterno(recepcionista.getApellidoPaterno());
        eRecepcionista.setCodigoPostal(recepcionista.getCodigoPostal());
        eRecepcionista.setCurp(recepcionista.getCurp());
        eRecepcionista.setDireccion(recepcionista.getDireccion());
        eRecepcionista.setEstado(recepcionista.getEstado());
        eRecepcionista.setFechaNacimiento(recepcionista.getFechaNacimiento());
        eRecepcionista.setLocalidad(recepcionista.getLocalidad());
        eRecepcionista.setNombre(recepcionista.getNombre());
        eRecepcionista.setNumTelefono(recepcionista.getNumTelefono());
        
        return eRecepcionista;
    }
    
    public static Cola colaAEntidad(hospital.core.model.Cola cola){
        Cola eCola = new Cola();
        List<Paciente> listaPacientes = new ArrayList<>();
        cola.getPacientes().forEach((paciente) -> {
            listaPacientes.add(pacienteAEntidad(paciente));
        });
        eCola.setPacienteList(listaPacientes);
        eCola.setTamanio(cola.getTamanio());
        eCola.setIdcola(cola.getId());
        
        return eCola;
    }
    
    public static Consulta consultaAEntidad(hospital.core.model.Consulta consulta){
        Consulta eConsulta = new Consulta();
        eConsulta.setFechaYHora(consulta.getFechaYHora());
        eConsulta.setIdmedico(medicoAEntidad(consulta.getMedico()));
        eConsulta.setIdpaciente(pacienteAEntidad(consulta.getPaciente()));
        eConsulta.setIdconsulta(consulta.getId());
        return eConsulta;
    }
    
    public static Medicamento medicamentoAEntidad(hospital.core.model.Medicamento medicamento){
        Medicamento eMedicamento = new Medicamento();
        eMedicamento.setComponenteActivo(medicamento.getComponenteActivo());
        eMedicamento.setIdmedicamento(medicamento.getId());
        eMedicamento.setNombre(medicamento.getNombre());
        eMedicamento.setPresentacion(medicamento.getPresentacion());
        return eMedicamento;
    }
    
    public static Receta recetaAEntidad(hospital.core.model.Receta receta){
        List<Medicamento> listaEMedicamentos = new ArrayList<>();
        receta.getMedicamentos().forEach((medicamento) -> {
            listaEMedicamentos.add(ClaseAEntidad.medicamentoAEntidad(medicamento));
        });
        Receta eReceta = new Receta();
        eReceta.setEstatura(receta.getEstatura());
        eReceta.setIdreceta(receta.getId());
        eReceta.setIndicacion(receta.getIndicacion());
        eReceta.setMedicamentoList(listaEMedicamentos);
        eReceta.setPeso(receta.getPeso());
        eReceta.setPresion(receta.getPresion());
        
        return eReceta;
    }
    
    public static Consultorio consultorioAEntidad(hospital.core.model.Consultorio consultorio){
        Consultorio eConsultorio = new Consultorio();
        eConsultorio.setIdcola(colaAEntidad(consultorio.getCola()));
        eConsultorio.setIdconsultorio(consultorio.getId());
        eConsultorio.setNumero(consultorio.getNumero());
        
        return eConsultorio;
    }
}
