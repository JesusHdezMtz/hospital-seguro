/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hospitaljpa;

import hospital.core.model.Medico;
import hospital.core.model.Paciente;
import hospital.core.model.Seguro;
import hospital.core.model.Usuario;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Date;
import persistencia.entidades.Medicamento;
import persistencia.interfaces.clases.MedicamentoInterfaz;
import persistencia.interfaces.clases.MedicoInterfaz;
import persistencia.interfaces.clases.PacienteInterfaz;
import persistencia.interfaces.clases.SeguroInterfaz;
import persistencia.interfaces.clases.UsuarioInterfaz;

/**
 *
 * @author andres
 */
public class HospitalJPA {

    /**
     * @param args the command line arguments
     * @throws java.security.NoSuchAlgorithmException
     * @throws java.io.UnsupportedEncodingException
     */
    public static void main(String[] args) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        /*
        Seguro seguroConsult = new Seguro(new SeguroInterfaz());
        Seguro result = seguroConsult.buscarSeguro(1);
        System.out.println(result.getId() + "  " + result.getNombre());
        
        Medico medico = new Medico();
        Usuario usuario = new Usuario("Xalapa2001",hashPassword("sahda") , 2, new UsuarioInterfaz());
        medico.setApellidoMaterno("Landa");
        medico.setApellidoPaterno("Aguilar<");
        medico.setCodigoPostal("9123");
        medico.setCurp("SADF");
        medico.setDireccion("ADA as as as 10");
        medico.setEstado("Veracruz");
        medico.setFechaNacimiento(new Date()); 
        medico.setUsuario(usuario);
        medico.setLocalidad("Xalapa");
        medico.setNombre("Carolina");
        medico.setNumTelefono("123456789");
        medico.setCedulaProfesional(4545454);
        medico.setPersistencia(new MedicoInterfaz());
        medico.setDisponibilidad(true);
 
        medico.nuevoMedico();
*/
        /*
        hospital.core.model.Usuario user = new hospital.core.model.Usuario(new UsuarioInterfaz());
        user.setUsuario("admi");
        user.setContrasena(hashPassword("admi"));
        user.setTipoAcceso(3);
        user.nuevoUsuario();

        hospital.core.model.Medicamento medi = new hospital.core.model.Medicamento(new MedicamentoInterfaz());
        medi.setComponenteActivo("Ibuprofeno");
        medi.setNombre("Next");
        medi.setId(2);
        medi.setPresentacion("Tabletas 100mg");
        medi.editarMedicamento();
        
        */
    }

    
     public static String hashPassword(String contrasena) throws NoSuchAlgorithmException,
            UnsupportedEncodingException {
        MessageDigest sha256 = MessageDigest.getInstance("SHA-256");
        sha256.update(contrasena.getBytes("UTF-8"));
        byte[] digest = sha256.digest();
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < digest.length; i++) {
            sb.append(String.format("%02x", digest[i]));
        }
        return sb.toString();
    }
    
}
