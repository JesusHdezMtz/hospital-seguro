/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 *
 * @author andres
 */
public abstract class Contrasenas {
    public static String cifrarContra(String contra){
        MessageDigest sha256;
        try{
            sha256 = MessageDigest.getInstance("SHA-256");
            sha256.update(contra.getBytes("UTF-8"));
        } catch(NoSuchAlgorithmException | UnsupportedEncodingException ex){
            return null;
        }
        byte[] digest = sha256.digest();
        StringBuffer sb = new StringBuffer();
        for(int i=0; i < digest.length; i++){
            sb.append(String.format("%02x", digest[i]));
        }
        return sb.toString();
    }
}
