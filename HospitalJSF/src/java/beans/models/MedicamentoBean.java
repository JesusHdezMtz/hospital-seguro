/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans.models;

import hospital.core.model.Medicamento;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import persistencia.interfaces.clases.MedicamentoInterfaz;

/**
 * @author Jesús
 */
@ManagedBean
@RequestScoped
public class MedicamentoBean {

    private static List<Medicamento> medicamentosReceta = new ArrayList<>();
    private int idMedicamento;
    private Medicamento medicamento = new Medicamento(new MedicamentoInterfaz());

    public MedicamentoBean() {
    }

    public List<Medicamento> getMedicamentos() {
        Medicamento medicamento = new Medicamento(new MedicamentoInterfaz());
        return medicamento.todosMedicamentos();
    }

    public List<Medicamento> getMedicamentosReceta() {
        return medicamentosReceta;
    }

    public void setMedicamentosReceta(List<Medicamento> medicamentosReceta) {
        this.medicamentosReceta = medicamentosReceta;
    }

    public int getIdMedicamento() {
        return idMedicamento;
    }

    public void setIdMedicamento(int idMedicamento) {
        this.idMedicamento = idMedicamento;
    }

    public void agregarMedicamento() {
        if (!medicamentosReceta.isEmpty()) {
            medicamentosReceta.forEach((medicamento) -> {
                if (!medicamento.getNombre().equals(this.medicamento.buscarMedicamento(idMedicamento))) {
                    this.medicamentosReceta.add(medicamento.buscarMedicamento(idMedicamento));
                }
            });
        } else {
            this.medicamentosReceta.add(medicamento.buscarMedicamento(idMedicamento));
        }

    }

}
