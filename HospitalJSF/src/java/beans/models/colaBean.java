/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans.models;

import hospital.core.model.Cola;
import hospital.core.model.Consultorio;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import persistencia.interfaces.clases.ConsultorioInterfaz;

/**
 *
 * @author andres
 * colaBean
 */
@ManagedBean
@RequestScoped
public class colaBean {

    private Cola cola;
    private Consultorio consultorio;
    
    public colaBean() {
        consultorio = new Consultorio(0, new ConsultorioInterfaz());
        
    }

    public Cola getCola() {
        return cola;
    }

    public void setCola(Cola cola) {
        this.cola = cola;
    }

    public Consultorio getConsultorio() {
        return consultorio;
    }

    public void setConsultorio(Consultorio consultorio) {
        this.consultorio = consultorio;
    }
}
