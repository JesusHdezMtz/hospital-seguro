/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans.models;

import hospital.core.model.Usuario;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import persistencia.interfaces.clases.UsuarioInterfaz;
import utils.Contrasenas;

/**
 *
 * @author andres
 */
@ManagedBean(name="usuarioBean")
@RequestScoped
public class UsuarioBean {
    
    private Usuario usuario = new Usuario(new UsuarioInterfaz());
    
    public UsuarioBean() {
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }
    
    /***
     * 
     * @return -1 si el usuario no existe, 0 si las contraseñas no coinciden y
     * 1, 2 o 3 dependiendo del tipo de usuario
     */
    public String validarUsaurio(){
        Usuario usuarioRecuperado;
        usuarioRecuperado = usuario.buscarUsuario(usuario.getUsuario());
        
        if (usuarioRecuperado == null){
            return "inexistente";
        }
        
        if(usuarioRecuperado.getContrasena().equals(Contrasenas.cifrarContra(usuario.getContrasena()))){
            System.out.println(Contrasenas.cifrarContra(usuario.getContrasena()));
            switch(usuarioRecuperado.getTipoAcceso()){
                case 1:
                    return "recepcion";
                case 2:
                    return "consultorio";
                case 3:
                    return "administracion";
                default:
                    return "inexistente";
            }
        } else {
            return "missmatch";
        }
    }
    
    
}
