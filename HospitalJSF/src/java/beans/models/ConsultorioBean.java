/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans.models;

import hospital.core.model.Consultorio;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import persistencia.interfaces.clases.ConsultorioInterfaz;

/**
 *
 * @author andres
 * consultorioBean
 */
@ManagedBean
@RequestScoped
public class ConsultorioBean {

    private List<Consultorio> consultorios;
    private Consultorio buscador;
    private Consultorio recuperado;
    
    public ConsultorioBean() {
        buscador = new Consultorio(1, new ConsultorioInterfaz());
        consultorios = buscador.todosConsultorios();
        recuperado = buscador.buscarConsultorio(1);
    }

    public List<Consultorio> getConsultorios() {
        return consultorios;
    }

    public void setConsultorios(List<Consultorio> consultorios) {
        this.consultorios = consultorios;
    }

    public Consultorio getBuscador() {
        return buscador;
    }

    public void setBuscador(Consultorio buscador) {
        this.buscador = buscador;
    }
    
    public void actualizarTabla(){
        recuperado = buscador.buscarConsultorio(buscador.getId());
    }

    public Consultorio getRecuperado() {
        return recuperado;
    }

    public void setRecuperado(Consultorio recuperado) {
        this.recuperado = recuperado;
    }
}
