/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans.models;

import hospital.core.model.Paciente;
import hospital.core.model.Persona;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import persistencia.interfaces.clases.PacienteInterfaz;

/**
 *
 * @author Jesús
 */
@ManagedBean
@RequestScoped
public class PacienteBean {

    private Paciente paciente = new Paciente();
    private int id = 2;
    
    public PacienteBean() {
        paciente.setPersistencia(new PacienteInterfaz());
        paciente = paciente.buscarPaciente(id);
    }

    public Paciente getPaciente() {
        return paciente;
    }

    public void setPaciente(Paciente paciente) {
        this.paciente = paciente;
    }
    
    public String parseDate(){
        DateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        return format.format(paciente.getFechaNacimiento());
    }
    
}
