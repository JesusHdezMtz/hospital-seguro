/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hospital.core.model;

import hospital.core.interfaces.IPersistencia;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Jesús
 */
public class Consulta {
    private Date fechaYHora;
    private Medico medico;
    private Paciente paciente;
    private final IPersistencia persistencia;
    private Receta receta;
    private int id;

    public IPersistencia getPersistencia() {
        return persistencia;
    }

    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    
    public Consulta(IPersistencia persistencia){
        this.persistencia = persistencia;
    }

    public Consulta(Date fechaYHora, Medico doctor, 
            Paciente paciente, Receta receta, int id, IPersistencia persistencia) {
        this.fechaYHora = fechaYHora;
        this.medico = doctor;
        this.paciente = paciente;
        this.receta = receta;
        this.persistencia = persistencia;
        this.id = id;
    }
    
    public Consulta(Date fechaYHora, Medico doctor, 
            Paciente paciente, int id, IPersistencia persistencia) {
        this.fechaYHora = fechaYHora;
        this.medico = doctor;
        this.paciente = paciente;
        this.persistencia = persistencia;
        this.id = id;
    }

    public Date getFechaYHora() {
        return fechaYHora;
    }

    public void setFechaYHora(Date fechaYHora) {
        this.fechaYHora = fechaYHora;
    }

    public Medico getMedico() {
        return medico;
    }

    public void setMedico(Medico doctor) {
        this.medico = doctor;
    }

    public Paciente getPaciente() {
        return paciente;
    }

    public void setPaciente(Paciente paciente) {
        this.paciente = paciente;
    }
    
    public void nuevaConsulta(){
        persistencia.nuevo(this);
    }
    
    public List<Consulta> recuperarConsultasPorId(Integer id){
        return persistencia.recuperarPorId(id);
    }
    
    public void editarConsulta(){
        persistencia.editar(this);
    }
    
    public void eliminarConsulta(){
        persistencia.eliminar(id);
    }
    
    public int cantidadConsulta(){
        return persistencia.cantidad();
    }
    
    public Consulta buscarConsulta(Integer id){
        return (Consulta)persistencia.buscar(id);
    }
    
    public List<Consulta> todasConsultas(){
        return persistencia.todos();
    }

    public Receta getReceta() {
        return receta;
    }

    public void setReceta(Receta receta) {
        this.receta = receta;
    }
}
