/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hospital.core.model;

import hospital.core.interfaces.IPersistencia;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Jesús
 */
public class Recepcionista extends Persona{
    
    private int numPersonal;
    private Usuario usuario;

    public Recepcionista(String apellidoMaterno, String apellidoPaterno,
            String codigoPostal, String curp, String direccion, String estado,
            Date fechaNacimiento, String localidad, String nombre,
            String numTelefono, int numPersonal, Usuario usuario, IPersistencia persistencia) {
        super(apellidoMaterno, apellidoPaterno, codigoPostal, curp, direccion,
                estado, fechaNacimiento, localidad, nombre, numTelefono, persistencia);
        this.numPersonal = numPersonal;
        this.usuario = usuario;
    }
    public Recepcionista(){}
    
    public int getNumPersonal() {
        return numPersonal;
    }

    public void setNumPersonal(int numPersonal) {
        this.numPersonal = numPersonal;
    }
    
    public void nuevoRecepcionista(){
        persistencia.nuevo(this);
    }
    
    public void editarRecepcionista(){
        persistencia.editar(this);
    }
    
    public void eliminarRecepcionista(){
        persistencia.eliminar(id);
    }
    
    public List<Recepcionista> todosRecepcionista(){
        return persistencia.todos();
    } 
    
    public int cantidadRecepcionista(){
        return persistencia.cantidad();
    }
    
    public Recepcionista buscarRecepcionista(Integer id){
        return (Recepcionista) persistencia.buscar(id);
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }
    
}
