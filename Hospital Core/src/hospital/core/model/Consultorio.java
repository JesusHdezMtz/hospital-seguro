/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hospital.core.model;

import hospital.core.interfaces.IPersistencia;
import java.util.List;

/**
 *
 * @author Jesús
 */
public class Consultorio {
    
    private int numero;
    private Cola cola;
    private final IPersistencia persistencia;
    private int id;
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Consultorio(IPersistencia persistencia) {
        this.persistencia = persistencia;
    }        

    public Consultorio(int numero, IPersistencia persistencia) {
        this.numero = numero;
        this.persistencia = persistencia;
    }

    public Consultorio(int numero, Cola cola, IPersistencia persistencia) {
        this.numero = numero;
        this.cola = cola;
        this.persistencia = persistencia;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }
    
    public void nuevoConsultorio(){
        persistencia.nuevo(this);
    }
    
    public void editarConsultorio(){
        persistencia.editar(this);
    }
    
    public void eliminarConsultorio(){
        persistencia.eliminar(id);
    }
    
    public int cantidadConsultorios(){
        return persistencia.cantidad();
    }
    
    public Consultorio buscarConsultorio(Integer id){
        return (Consultorio) persistencia.buscar(id);
    }
    
    public List<Consultorio> todosConsultorios(){
        return persistencia.todos();
    }

    public Cola getCola() {
        return cola;
    }

    public void setCola(Cola cola) {
        this.cola = cola;
    }
    
    @Override
    public String toString(){
        return Integer.toString(getNumero());
    }
}
