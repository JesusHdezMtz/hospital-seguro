package hospital.core.model;

import hospital.core.interfaces.IPersistencia;
import java.util.List;

public class Medicamento {

    private String componenteActivo;
    private String nombre;
    private String Presentacion;
    private IPersistencia persistencia;
    private int id;
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Medicamento(IPersistencia persistencia) {
        this.persistencia = persistencia;
    }
    
    public Medicamento(String componenteActivo, String nombre, 
            String Presentacion, IPersistencia persistencia) {
        this.componenteActivo = componenteActivo;
        this.nombre = nombre;
        this.Presentacion = Presentacion;
        this.persistencia = persistencia;
    }

    public String getComponenteActivo() {
        return componenteActivo;
    }

    public void setComponenteActivo(String componenteActivo) {
        this.componenteActivo = componenteActivo;
    }

    public String getNombre() {
        return nombre;
    }
    
    public IPersistencia getPersistencia() {
        return persistencia;
    }

    public void setPersistencia(IPersistencia persistencia) {
        this.persistencia = persistencia;
    }
    
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPresentacion() {
        return Presentacion;
    }

    public void setPresentacion(String Presentacion) {
        this.Presentacion = Presentacion;
    }
    
    public void nuevoMedicamento(){
        persistencia.nuevo(this);
    }
    
    public void editarMedicamento(){
        persistencia.editar(this);
    }
    
    public void eliminarMedicamento(){
        persistencia.eliminar(id);
    }
    
    public int cantidadMedicamento(){
        return persistencia.cantidad();
    }
    
    public Medicamento buscarMedicamento(Integer id){
        return (Medicamento) persistencia.buscar(id);
    }
    
    public List<Medicamento> todosMedicamentos(){
        return persistencia.todos();
    }
}
