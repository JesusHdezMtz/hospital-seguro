package hospital.core.model;

import hospital.core.interfaces.IPersistencia;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Jesús
 */
public class Medico extends Persona{
    
    private int cedulaProfesional;
    private boolean disponibilidad;
    private Usuario usuario;
    
    public Medico(){}
    
    public Medico(String apellidoMaterno, String apellidoPaterno, 
            String codigoPostal, String curp, String direccion, String estado, 
            Date fechaNacimiento, String localidad, String nombre, 
            String numTelefono, int cedula, boolean disp, Usuario usuario, IPersistencia persistencia) {
        
        super(apellidoMaterno, apellidoPaterno, codigoPostal, curp, direccion,
                estado, fechaNacimiento, localidad, nombre, numTelefono, persistencia);
        cedulaProfesional = cedula;
        disponibilidad = disp;
        this.usuario = usuario;
    }

    public int getCedulaProfesional() {
        return cedulaProfesional;
    }

    public void setCedulaProfesional(int cedulaProfesional) {
        this.cedulaProfesional = cedulaProfesional;
    }

    public boolean getDisponibilidad() {
        return disponibilidad;
    }
    
    public void setDisponibilidad(boolean disponibilidad){
        this.disponibilidad = disponibilidad;
    }
    
    public void nuevoMedico(){
        persistencia.nuevo(this);
    }
    
    public void editarMedico(){
        persistencia.editar(this);
    }
    
    public void eliminarMedico(){
        persistencia.eliminar(id);
    }
    
    public List<Medico> todosMedicos(){
        return persistencia.todos();
    }
    
    public int medicosCantidad(){
        return persistencia.cantidad();
    }
    
    public Medico buscarMedico(Integer id){
        return (Medico) persistencia.buscar(id);
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }
    
    
}
