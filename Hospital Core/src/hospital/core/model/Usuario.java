package hospital.core.model;

import hospital.core.interfaces.IPersistencia;
import java.util.List;

/**
 *
 * @author Jesús
 */
public class Usuario {
    protected String usuario;
    protected String contrasena;
    protected int tipoAcceso;
    protected IPersistencia persistencia;

    public Usuario(){}

    public Usuario(IPersistencia persistencia){
        this.persistencia = persistencia;
    }
    
    public Usuario(String usuario, String contrasena, int tipoAcceso, IPersistencia persistencia) {
        this.usuario = usuario;
        this.contrasena = contrasena;
        this.tipoAcceso = tipoAcceso;
        this.persistencia = persistencia;
    }
    
    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getContrasena() {
        return contrasena;
    }

    public void setContrasena(String contrasena) {
        this.contrasena = contrasena;
    }

    public int getTipoAcceso() {
        return tipoAcceso;
    }

    public void setTipoAcceso(int tipoAcceso) {
        this.tipoAcceso = tipoAcceso;
    }

    public IPersistencia getPersistencia() {
        return persistencia;
    }

    public void setPersistencia(IPersistencia persistencia) {
        this.persistencia = persistencia;
    }
    
    public void nuevoUsuario(){
        persistencia.nuevo(this);
    }
    
    public void editarUsuario(){
        persistencia.editar(this);
    }
    
    public void eliminarUsuario(){
        persistencia.eliminarPorNombre(usuario);
    }
    
    public List<Usuario> todosUsuario(){
        return persistencia.todos();
    } 
    
    public int cantidadUsuario(){
        return persistencia.cantidad();
    }
    
    public Usuario buscarUsuario(String nombre){
        return (Usuario) persistencia.buscarPorNombre(nombre);
    }
    
}
