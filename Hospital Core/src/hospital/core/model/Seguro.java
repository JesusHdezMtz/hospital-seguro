package hospital.core.model;

import hospital.core.interfaces.IPersistencia;
import java.util.List;

public class Seguro {
    private String nombre;
    private final IPersistencia persistencia;
    private int id;
    
    public Seguro(IPersistencia persistencia) {
        this.persistencia = persistencia;
    }        
       
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }       
        
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
    public void nuevoSeguro(){
        persistencia.nuevo(this);
    }
    
    public void editarSeguro(){
        persistencia.editar(this);
    }
    
    public void eliminarSeguro(){
        persistencia.eliminar(id);
    }
    
    public List<Seguro> todosSeguro(){
        return persistencia.todos();
    }
    
    public int cantidadSeguro(){
        return persistencia.cantidad();
    }
    
    public Seguro buscarSeguro(Integer id){
        return (Seguro) persistencia.buscar(id);
    }
    
    @Override
    public String toString(){
        return getNombre();
    }
}
