package hospital.core.model;

import hospital.core.interfaces.IPersistencia;
import java.util.Calendar;
import java.util.List;

public class Itinerario {
    private Medico medico;
    private Consultorio consultorio;
    private Calendar fecha;
    private IPersistencia persistencia;
    private int id;
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Itinerario(Medico medico, Consultorio consultorio, Calendar fecha,
            IPersistencia persistencia) {
        this.medico = medico;
        this.consultorio = consultorio;
        this.fecha = fecha;
        this.persistencia = persistencia;
    }

    public Medico getMedico() {
        return medico;
    }

    public void setMedico(Medico medico) {
        this.medico = medico;
    }

    public Consultorio getConsultorio() {
        return consultorio;
    }

    public void setConsultorio(Consultorio consultorio) {
        this.consultorio = consultorio;
    }

    public Calendar getFecha() {
        return fecha;
    }

    public void setFecha(Calendar fecha) {
        this.fecha = fecha;
    }
    
    public void nuevoItinerario(){
        persistencia.nuevo(this);
    }
    
    public void editarItinerario(){
        persistencia.editar(this);
    }
    
    public void eliminarItinerario(){
        persistencia.eliminar(id);
    }
    
    public int cantidadItinerario(){
        return persistencia.cantidad();
    }
    
    public Itinerario buscarItinerario(Integer id){
        return (Itinerario) persistencia.buscar(id);
    }
    
    public List<Itinerario> todosItinerarios(){
        return persistencia.todos();
    }
}
