package hospital.core.model;

import hospital.core.interfaces.IPersistencia;
import java.util.Date;
import java.util.List;


public class Paciente extends Persona{
    
    private int numeroSeguro;
    private Seguro seguro;

    public Paciente() {
    }
    
    public Paciente(IPersistencia persistencia) {
        this.persistencia = persistencia;
    }
    
    public Paciente(String apellidoMaterno, String apellidoPaterno, 
            String codigoPostal, String curp, String direccion, String estado,
            Date fechaNacimiento, String localidad, String nombre,
            String numTelefono, int numeroSeguro, Seguro seguro, 
            IPersistencia persistencia) {
        super(apellidoMaterno, apellidoPaterno, codigoPostal, curp, direccion,
                estado, fechaNacimiento, localidad, nombre, numTelefono, persistencia);
        this.numeroSeguro = numeroSeguro;
        this.seguro = seguro;
    }

    public int getNumeroSeguro() {
        return numeroSeguro;
    }

    public void setNumeroSeguro(int numeroSeguro) {
        this.numeroSeguro = numeroSeguro;
    }

    public Seguro getSeguro() {
        return seguro;
    }

    public void setSeguro(Seguro seguro) {
        this.seguro = seguro;
    }
    
    public void nuevoPaciente(){
        persistencia.nuevo(this);
    }
    
    public void editarPaciente(){
        persistencia.editar(this);
    }
    
    public void eliminarPaciente(){
        persistencia.eliminar(id);
    }
    
    public List<Paciente> todosPaciente(){
        return persistencia.todos();
    }
    
    public int cantidadPaciente(){
        return persistencia.cantidad();
    }
    
    public Paciente buscarPaciente(Integer id){
        return (Paciente) persistencia.buscar(id);
    }
    
    public List<Paciente> buscarPorNombre(String nombre){
        return (List<Paciente>) persistencia.buscarPorNombre(nombre);
    }    
}
