package hospital.core.model;

import hospital.core.interfaces.IPersistencia;
import java.util.ArrayList;
import java.util.List;

public class Receta {
    private float estatura;
    private String indicacion;
    private float peso;
    private String presion;
    private List<Medicamento> medicamentos;
    private final IPersistencia persistencia;
    private int id;

    public Receta(IPersistencia persistencia) {
        this.persistencia = persistencia;
    }
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Receta(float estatura, String indicacion, float peso, 
            String presion, IPersistencia persistencia) {
        this.estatura = estatura;
        this.indicacion = indicacion;
        this.peso = peso;
        this.presion = presion;
        this.medicamentos = new ArrayList();
        this.persistencia = persistencia;
    }

    public Receta(float estatura, String indicacion, float peso, String presion, 
            ArrayList<Medicamento> medicamentos, IPersistencia persistencia) {
        this.estatura = estatura;
        this.indicacion = indicacion;
        this.peso = peso;
        this.presion = presion;
        this.medicamentos = medicamentos;
        this.persistencia = persistencia;
    }
    
    public float getEstatura() {
        return estatura;
    }

    public void setEstatura(float estatura) {
        this.estatura = estatura;
    }

    public String getIndicacion() {
        return indicacion;
    }

    public void setIndicacion(String indicacion) {
        this.indicacion = indicacion;
    }

    public float getPeso() {
        return peso;
    }

    public void setPeso(float peso) {
        this.peso = peso;
    }

    public String getPresion() {
        return presion;
    }

    public void setPresion(String presion) {
        this.presion = presion;
    }
    
    public void agregarMedicamento(Medicamento medicamento){
        this.medicamentos.add(medicamento);
    }
    
    public void nuevaReceta(){
        persistencia.nuevo(this);
    }
    
    public void editarReceta(){
        persistencia.editar(this);
    }
    
    public void eliminarReceta(){
        persistencia.eliminar(id);
    }
    
    public List<Receta> todasRecetas(){
        return persistencia.todos();
    }
    
    public int cantidadRecetas(){
        return persistencia.cantidad();
    }
    
    public Receta buscarReceta(Integer id){
        return (Receta) persistencia.buscar(id);
    }

    public List<Medicamento> getMedicamentos() {
        return medicamentos;
    }

    public void setMedicamentos(List<Medicamento> medicamentos) {
        this.medicamentos = medicamentos;
    }
    
    public List<Receta> recuperarRecetasPaciente(Integer id){
        return persistencia.recuperarPorId(id);
    }
}
