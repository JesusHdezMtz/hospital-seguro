/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hospital.core.model;

import hospital.core.interfaces.IPersistencia;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Jesús
 */
public class Cola {
    
    private int tamanio;
    private final List<Paciente> pacientes;
    private final IPersistencia persistencia;
    private int id;
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Cola(IPersistencia persistencia) {
        this.pacientes = new ArrayList<>();
        this.persistencia = persistencia;
        this.tamanio = pacientes.size();
    }

    public Cola(IPersistencia persistencia, List<Paciente> pacientes) {
        this.tamanio = pacientes.size();
        this.persistencia = persistencia;
        this.pacientes = pacientes;
    }
    
    public void agregar(Paciente paciente){
        this.pacientes.add(paciente);
    }
    
    public void mandarALaCola(Paciente paciente){
        this.pacientes.remove(paciente);
        this.pacientes.add(paciente);
    }
    
    public void removerPersona(Paciente paciente){
        this.pacientes.remove(paciente);
    }
    
    public int getTamanio() {
        return tamanio;
    }

    public List<Paciente> getPacientes() {
        return pacientes;
    }
    
    public void nuevaCola(){
        persistencia.nuevo(this);
    }
    
    
    private void editarCola(){
        persistencia.editar(this);
    }

    public void eliminarCola(){
        persistencia.eliminar(this.id);
    }
    
    public Cola buscarCola(Integer id){
        return (Cola) persistencia.buscar(id);
    }
    
    public List<Cola> todasColas(){
        return persistencia.todos();
    }
    
    public int cantidadCola() {
        return persistencia.cantidad();
    }
    
    public void setTamanio(int tamanio){
        this.tamanio = tamanio;
    }
    
}
