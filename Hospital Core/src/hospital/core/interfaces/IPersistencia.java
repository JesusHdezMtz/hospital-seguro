/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hospital.core.interfaces;

import java.util.List;

/**
 *
 * @author andres
 * @param <T> Cualquier Entidad que vaya a ser utilizada
 */
public interface IPersistencia <T>{
    
    public void nuevo(T objeto);
    public void editar(T objeto);
    public void eliminar(Integer id);
    public List<T> todos();
    public int cantidad();
    public T buscar(Integer id);
    public T buscarPorNombre(String busqueda);
    public void eliminarPorNombre(String eliminar);
    public List<T> recuperarPorId(Integer id);
}
